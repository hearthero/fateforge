![](https://i.imgur.com/ZhxRRTF.png)

FateForge is an editor for Fire Emblem Fates that has been mainly worked on at the end of 2018 / start of 2019. The goal was to port tools for Fates like FEFLib to C# and to provide one editor with powerful functionality that compiles directly for faster play testing. 

![enter image description here](https://i.imgur.com/SlZ2uYD.png)
![enter image description here](https://i.imgur.com/GafY4da.png)

Back in 2019 I worked on a hack called FE:Mirrors that changed the story and visuals of Fire Emblem Fates, added new classes and animations , new textures, music and even new gameplay elements. Each character also swapped color schemes with counterparts/siblings.
| ![](https://i.imgur.com/eUOxJxg.png) | ![](https://i.imgur.com/4eKxvwk.png) |
|--|--|

I also uploaded [images of all character color swaps to Google drive](https://drive.google.com/drive/folders/1ymXOv4FRxITmRqCRin0kbNap6bg-nuE7?usp=sharing) and you can do whatever you want with them.

Use them for a hack or meme with them - there is some good potential. ;D

## The future of this project
Even if all of this might sound cool it has been discontinued by me. The main reason is a hdd crash that occured in 2019 and killed most of the progress. All I could restore was a pretty early prototype of FateForge with many flaws. It was pretty depressing and I kept what remains for almost two years now so I decided it would be time to change that. This software should be open source so that others can do something with the remains of FateForge.

I don't know if I ever come back to this project but there is some code that people should be able to work with. All it needs is an ambitious programmer with C# knowledge that is willing to create something new with the existing code.

Feel free to fork the project and have some fun. I won't give any support though - you have to figure out everything yourself. As a starting point you can use HackingToolkit3DS to extract the contents of a Fates ROM and add a `.ffp` file to the location containing this:
```
{
  "Name": "HACK NAME",
  "Dir": "PATH TO THE DIRECTORY",
  "Init": 0,
  "Settings": []
}
```

That's it from my side. 
