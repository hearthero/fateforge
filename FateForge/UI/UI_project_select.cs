﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace FateForge
{
    public partial class UI_project_select : Form
    {

        StringCollection last_projects = Properties.Settings.Default.last_projects;

        public UI_project_select()
        {
            InitializeComponent();
        }

        private void InitMain()
        {
            if (!last_projects.Contains(Globals.Project.Dir + Path.DirectorySeparatorChar + Globals.Project.Name.ToLower() + ".ffp"))
            {
                last_projects.Insert(0,Globals.Project.Dir + Path.DirectorySeparatorChar + Globals.Project.Name.ToLower() + ".ffp");
                Properties.Settings.Default.last_projects = last_projects;
                Properties.Settings.Default.Save();
            }

            Globals.Folder_Encrypted = Path.Combine(Globals.Project.Dir, Globals.Project.Name + "Decrypted");
            Globals.Folder_Decrypted = Path.Combine(Globals.Project.Dir, "ExtractedRomFS", "GameData", "Dispos") + Path.DirectorySeparatorChar;

            Main main = new Main();
            Hide();
            main.ShowDialog();
            Close();
        }

        private void btn_create_Click(object sender, EventArgs e)
        {


        }

        private void btn_open_Click(object sender, EventArgs e)
        {
            if (open_project.ShowDialog() == DialogResult.OK)
            {
                string json = File.ReadAllText(open_project.FileName);
                Globals.Project = JsonConvert.DeserializeObject<FFP>(json);

                InitMain();

            }
        }

        private void list_projects_DoubleClick(object sender, EventArgs e)
        {
            if (list_projects.SelectedItem != null)
            {
                string json = File.ReadAllText(list_projects.SelectedItem.ToString());
                Globals.Project = JsonConvert.DeserializeObject<FFP>(json);

                InitMain();
            }
        }

        private void UI_project_select_Load(object sender, EventArgs e)
        {
            if (last_projects != null)
            {
                foreach (string project in last_projects)
                {
                    list_projects.Items.Add(project);
                }
            }
            else
            {
                last_projects = new StringCollection();
            }
        }
    }
}
