﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FEFLib.gamedata.dispos;

namespace FateForge.UI.Controls
{
    public partial class Dispos_Panel : UserControl
    {
        public Dispos_Panel()
        {
            InitializeComponent();
        }

        public void LoadDispos(DisposBlock block)
        {
            CORE_PID.Text = block.getPid();
            CORE_TEAM.Value = block.getTeam();
            CORE_LEVEL.Value = block.getLevel();

            CORE_X1.Value = block.getFirstCoord()[0];
            CORE_X2.Value = block.getFirstCoord()[1];
            CORE_Y1.Value = block.getSecondCoord()[0];
            CORE_Y2.Value = block.getSecondCoord()[1];

            AI_SPAWN_1.Value = block.getSpawnBitflags()[0];
            AI_SPAWN_2.Value = block.getSpawnBitflags()[1];
            AI_SPAWN_3.Value = block.getSpawnBitflags()[2];
            AI_SPAWN_4.Value = block.getSpawnBitflags()[3];

            AI_AC_FLAG.Text = block.getAc();
            AI_MI_FLAG.Text = block.getMi();
            AI_AT_FLAG.Text = block.getAt();
            AI_MV_FLAG.Text = block.getMv();

            AI_AC_PARAMETER.Text = block.getAiPositionOne();
            AI_MI_PARAMETER.Text = block.getAiPositionTwo();
            AI_AT_PARAMETER.Text = block.getAiPositionThree();
            AI_MV_PARAMETER.Text = block.getAiPositionFour();

        }
    }
}
