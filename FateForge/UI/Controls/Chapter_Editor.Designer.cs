﻿namespace FateForge.UI.Controls
{
    partial class Chapter_Editor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chapter_Editor));
            this.chapter_tabs = new System.Windows.Forms.TabControl();
            this.page_terrain = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.terrain_effect = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.terrain_heal = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.terrain_avo = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.terrain_def = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.terrain_c1 = new System.Windows.Forms.NumericUpDown();
            this.terrain_c2 = new System.Windows.Forms.NumericUpDown();
            this.terrain_c3 = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.terrain_mtid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.terrain_tid = new System.Windows.Forms.TextBox();
            this.terrain_editor = new Ted.TileEditor.TileEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tile_dragonvein = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.terrain_tiles = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.map_model = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.map_border_y = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.map_border_x = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.map_size_y = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.map_size_x = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.page_spawns = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.spawn_tree = new System.Windows.Forms.TreeView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dispos_character = new FateForge.UI.Controls.Dispos_Panel();
            this.page_persons = new System.Windows.Forms.TabPage();
            this.page_scripts = new System.Windows.Forms.TabPage();
            this.SCRIPT = new ScintillaNET.Scintilla();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.chapterScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terrainScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.script_linecount = new System.Windows.Forms.ToolStripLabel();
            this.page_messages = new System.Windows.Forms.TabPage();
            this.MESS_content = new ScintillaNET.Scintilla();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.MESS_NUMBER = new System.Windows.Forms.Label();
            this.MESS_combobox = new System.Windows.Forms.ComboBox();
            this.MESS_add = new System.Windows.Forms.Button();
            this.chapter_tabs.SuspendLayout();
            this.page_terrain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_heal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_avo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_def)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c3)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.map_border_y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_border_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_size_y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_size_x)).BeginInit();
            this.page_spawns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.page_scripts.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.page_messages.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // chapter_tabs
            // 
            this.chapter_tabs.Controls.Add(this.page_terrain);
            this.chapter_tabs.Controls.Add(this.page_spawns);
            this.chapter_tabs.Controls.Add(this.page_persons);
            this.chapter_tabs.Controls.Add(this.page_scripts);
            this.chapter_tabs.Controls.Add(this.page_messages);
            this.chapter_tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chapter_tabs.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chapter_tabs.Location = new System.Drawing.Point(0, 0);
            this.chapter_tabs.Name = "chapter_tabs";
            this.chapter_tabs.SelectedIndex = 0;
            this.chapter_tabs.Size = new System.Drawing.Size(640, 447);
            this.chapter_tabs.TabIndex = 0;
            // 
            // page_terrain
            // 
            this.page_terrain.Controls.Add(this.splitContainer1);
            this.page_terrain.Controls.Add(this.panel1);
            this.page_terrain.Location = new System.Drawing.Point(4, 25);
            this.page_terrain.Name = "page_terrain";
            this.page_terrain.Padding = new System.Windows.Forms.Padding(3);
            this.page_terrain.Size = new System.Drawing.Size(632, 418);
            this.page_terrain.TabIndex = 0;
            this.page_terrain.Text = "Terrain";
            this.page_terrain.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 129);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(6);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.terrain_editor);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.splitContainer1.Size = new System.Drawing.Size(626, 286);
            this.splitContainer1.SplitterDistance = 270;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.terrain_effect);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.terrain_heal);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.terrain_avo);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.terrain_def);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.terrain_c1);
            this.groupBox3.Controls.Add(this.terrain_c2);
            this.groupBox3.Controls.Add(this.terrain_c3);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.terrain_mtid);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.terrain_tid);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 274);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Selected Tile";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 16);
            this.label14.TabIndex = 31;
            this.label14.Text = "EFFECT";
            // 
            // terrain_effect
            // 
            this.terrain_effect.Location = new System.Drawing.Point(73, 101);
            this.terrain_effect.Name = "terrain_effect";
            this.terrain_effect.Size = new System.Drawing.Size(134, 26);
            this.terrain_effect.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 16);
            this.label13.TabIndex = 29;
            this.label13.Text = "HEAL";
            // 
            // terrain_heal
            // 
            this.terrain_heal.Location = new System.Drawing.Point(73, 207);
            this.terrain_heal.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_heal.Name = "terrain_heal";
            this.terrain_heal.Size = new System.Drawing.Size(134, 26);
            this.terrain_heal.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 177);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 16);
            this.label12.TabIndex = 27;
            this.label12.Text = "AVO";
            // 
            // terrain_avo
            // 
            this.terrain_avo.Location = new System.Drawing.Point(73, 172);
            this.terrain_avo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_avo.Name = "terrain_avo";
            this.terrain_avo.Size = new System.Drawing.Size(134, 26);
            this.terrain_avo.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 16);
            this.label11.TabIndex = 25;
            this.label11.Text = "DEF";
            // 
            // terrain_def
            // 
            this.terrain_def.Location = new System.Drawing.Point(73, 137);
            this.terrain_def.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_def.Name = "terrain_def";
            this.terrain_def.Size = new System.Drawing.Size(134, 26);
            this.terrain_def.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 16);
            this.label10.TabIndex = 23;
            this.label10.Text = "Change";
            // 
            // terrain_c1
            // 
            this.terrain_c1.Location = new System.Drawing.Point(73, 247);
            this.terrain_c1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_c1.Name = "terrain_c1";
            this.terrain_c1.Size = new System.Drawing.Size(40, 26);
            this.terrain_c1.TabIndex = 22;
            // 
            // terrain_c2
            // 
            this.terrain_c2.Location = new System.Drawing.Point(119, 247);
            this.terrain_c2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_c2.Name = "terrain_c2";
            this.terrain_c2.Size = new System.Drawing.Size(40, 26);
            this.terrain_c2.TabIndex = 21;
            // 
            // terrain_c3
            // 
            this.terrain_c3.Location = new System.Drawing.Point(167, 247);
            this.terrain_c3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.terrain_c3.Name = "terrain_c3";
            this.terrain_c3.Size = new System.Drawing.Size(40, 26);
            this.terrain_c3.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "MTID";
            // 
            // terrain_mtid
            // 
            this.terrain_mtid.Enabled = false;
            this.terrain_mtid.Location = new System.Drawing.Point(73, 66);
            this.terrain_mtid.Name = "terrain_mtid";
            this.terrain_mtid.Size = new System.Drawing.Size(134, 26);
            this.terrain_mtid.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "TID";
            // 
            // terrain_tid
            // 
            this.terrain_tid.Location = new System.Drawing.Point(73, 31);
            this.terrain_tid.Name = "terrain_tid";
            this.terrain_tid.Size = new System.Drawing.Size(134, 26);
            this.terrain_tid.TabIndex = 16;
            // 
            // terrain_editor
            // 
            this.terrain_editor.AllowDrop = true;
            this.terrain_editor.AllowSelect = false;
            this.terrain_editor.AutoScroll = true;
            this.terrain_editor.BackColor = System.Drawing.Color.White;
            this.terrain_editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.terrain_editor.HighlightTileUnderMouse = false;
            this.terrain_editor.Location = new System.Drawing.Point(3, 3);
            this.terrain_editor.MapHeight = 32;
            this.terrain_editor.MapLayers = 1;
            this.terrain_editor.MapSize = new System.Drawing.Size(32, 32);
            this.terrain_editor.MapWidth = 32;
            this.terrain_editor.Name = "terrain_editor";
            this.terrain_editor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.terrain_editor.Size = new System.Drawing.Size(346, 280);
            this.terrain_editor.TabIndex = 1;
            this.terrain_editor.TileHeight = 12;
            this.terrain_editor.TileSize = new System.Drawing.Size(12, 12);
            this.terrain_editor.TileSpacing = 0;
            this.terrain_editor.TileWidth = 12;
            this.terrain_editor.ViewingPoint = new System.Drawing.Point(0, 0);
            this.terrain_editor.MouseDown += new System.EventHandler<Ted.TileEditor.MouseTileEventArgs>(this.terrain_editor_MouseDown);
            this.terrain_editor.PaintTile += new System.EventHandler<Ted.TileEditor.TileEventArgs>(this.terrain_editor_PaintTile);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(626, 126);
            this.panel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tile_dragonvein);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.terrain_tiles);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 120);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tiles";
            // 
            // tile_dragonvein
            // 
            this.tile_dragonvein.AutoSize = true;
            this.tile_dragonvein.Location = new System.Drawing.Point(11, 87);
            this.tile_dragonvein.Name = "tile_dragonvein";
            this.tile_dragonvein.Size = new System.Drawing.Size(98, 20);
            this.tile_dragonvein.TabIndex = 4;
            this.tile_dragonvein.Text = "Dragon Vein";
            this.tile_dragonvein.UseVisualStyleBackColor = true;
            this.tile_dragonvein.CheckedChanged += new System.EventHandler(this.tile_dragonvein_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(223, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add Tile";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Selected Tile:";
            // 
            // terrain_tiles
            // 
            this.terrain_tiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.terrain_tiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.terrain_tiles.FormattingEnabled = true;
            this.terrain_tiles.Location = new System.Drawing.Point(115, 27);
            this.terrain_tiles.Name = "terrain_tiles";
            this.terrain_tiles.Size = new System.Drawing.Size(183, 24);
            this.terrain_tiles.TabIndex = 1;
            this.terrain_tiles.SelectedIndexChanged += new System.EventHandler(this.terrain_tiles_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.map_model);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.map_border_y);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.map_border_x);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.map_size_y);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.map_size_x);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(326, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(297, 120);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Map Settings";
            // 
            // map_model
            // 
            this.map_model.Location = new System.Drawing.Point(80, 27);
            this.map_model.Name = "map_model";
            this.map_model.Size = new System.Drawing.Size(206, 26);
            this.map_model.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "3D Model";
            // 
            // map_border_y
            // 
            this.map_border_y.Location = new System.Drawing.Point(80, 86);
            this.map_border_y.Name = "map_border_y";
            this.map_border_y.Size = new System.Drawing.Size(60, 26);
            this.map_border_y.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Border Y:";
            // 
            // map_border_x
            // 
            this.map_border_x.Location = new System.Drawing.Point(11, 86);
            this.map_border_x.Name = "map_border_x";
            this.map_border_x.Size = new System.Drawing.Size(60, 26);
            this.map_border_x.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Border X:";
            // 
            // map_size_y
            // 
            this.map_size_y.Location = new System.Drawing.Point(226, 86);
            this.map_size_y.Name = "map_size_y";
            this.map_size_y.Size = new System.Drawing.Size(60, 26);
            this.map_size_y.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(223, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Map Y:";
            // 
            // map_size_x
            // 
            this.map_size_x.Location = new System.Drawing.Point(152, 86);
            this.map_size_x.Name = "map_size_x";
            this.map_size_x.Size = new System.Drawing.Size(60, 26);
            this.map_size_x.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Map X:";
            // 
            // page_spawns
            // 
            this.page_spawns.Controls.Add(this.splitContainer4);
            this.page_spawns.Location = new System.Drawing.Point(4, 25);
            this.page_spawns.Name = "page_spawns";
            this.page_spawns.Padding = new System.Windows.Forms.Padding(3);
            this.page_spawns.Size = new System.Drawing.Size(632, 418);
            this.page_spawns.TabIndex = 1;
            this.page_spawns.Text = "Spawns";
            this.page_spawns.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.Location = new System.Drawing.Point(3, 3);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.AutoScroll = true;
            this.splitContainer4.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer4.Panel1.Controls.Add(this.tabControl2);
            this.splitContainer4.Size = new System.Drawing.Size(626, 412);
            this.splitContainer4.SplitterDistance = 240;
            this.splitContainer4.TabIndex = 2;
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(240, 412);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.spawn_tree);
            this.tabPage5.Location = new System.Drawing.Point(4, 28);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(232, 380);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Spawn Tree";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // spawn_tree
            // 
            this.spawn_tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spawn_tree.Location = new System.Drawing.Point(3, 3);
            this.spawn_tree.Name = "spawn_tree";
            this.spawn_tree.Size = new System.Drawing.Size(226, 374);
            this.spawn_tree.TabIndex = 0;
            this.spawn_tree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.spawn_tree_AfterSelect);
            this.spawn_tree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.spawn_tree_NodeMouseClick);
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.BackColor = System.Drawing.Color.White;
            this.tabPage6.Controls.Add(this.dispos_character);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(232, 380);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Character";
            // 
            // dispos_character
            // 
            this.dispos_character.AutoScroll = true;
            this.dispos_character.BackColor = System.Drawing.Color.White;
            this.dispos_character.Dock = System.Windows.Forms.DockStyle.Top;
            this.dispos_character.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dispos_character.Location = new System.Drawing.Point(3, 3);
            this.dispos_character.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dispos_character.Name = "dispos_character";
            this.dispos_character.Size = new System.Drawing.Size(209, 650);
            this.dispos_character.TabIndex = 0;
            // 
            // page_persons
            // 
            this.page_persons.Location = new System.Drawing.Point(4, 25);
            this.page_persons.Name = "page_persons";
            this.page_persons.Size = new System.Drawing.Size(632, 418);
            this.page_persons.TabIndex = 2;
            this.page_persons.Text = "Persons";
            this.page_persons.UseVisualStyleBackColor = true;
            // 
            // page_scripts
            // 
            this.page_scripts.Controls.Add(this.SCRIPT);
            this.page_scripts.Controls.Add(this.toolStrip1);
            this.page_scripts.Location = new System.Drawing.Point(4, 25);
            this.page_scripts.Name = "page_scripts";
            this.page_scripts.Size = new System.Drawing.Size(632, 418);
            this.page_scripts.TabIndex = 3;
            this.page_scripts.Text = "Scripts";
            this.page_scripts.UseVisualStyleBackColor = true;
            // 
            // SCRIPT
            // 
            this.SCRIPT.AdditionalCaretForeColor = System.Drawing.Color.DodgerBlue;
            this.SCRIPT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SCRIPT.CaretForeColor = System.Drawing.Color.White;
            this.SCRIPT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SCRIPT.EdgeColor = System.Drawing.Color.Black;
            this.SCRIPT.Location = new System.Drawing.Point(0, 25);
            this.SCRIPT.Name = "SCRIPT";
            this.SCRIPT.Size = new System.Drawing.Size(632, 393);
            this.SCRIPT.TabIndex = 2;
            this.SCRIPT.Text = "NO SCRIPT";
            this.SCRIPT.TextChanged += new System.EventHandler(this.SCRIPT_TextChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator2,
            this.script_linecount});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(632, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chapterScriptToolStripMenuItem,
            this.terrainScriptToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(51, 22);
            this.toolStripDropDownButton1.Text = "Mode";
            // 
            // chapterScriptToolStripMenuItem
            // 
            this.chapterScriptToolStripMenuItem.Checked = true;
            this.chapterScriptToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chapterScriptToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.chapterScriptToolStripMenuItem.Name = "chapterScriptToolStripMenuItem";
            this.chapterScriptToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.chapterScriptToolStripMenuItem.Text = "Chapter Script";
            // 
            // terrainScriptToolStripMenuItem
            // 
            this.terrainScriptToolStripMenuItem.Name = "terrainScriptToolStripMenuItem";
            this.terrainScriptToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.terrainScriptToolStripMenuItem.Text = "Terrain Script";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::FateForge.Properties.Resources.icon_chat;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Add Talk Event";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::FateForge.Properties.Resources.icon_chest;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Add Chest Event";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::FateForge.Properties.Resources.icon_red_flag;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Add Reinforcement";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // script_linecount
            // 
            this.script_linecount.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.script_linecount.Name = "script_linecount";
            this.script_linecount.Size = new System.Drawing.Size(55, 22);
            this.script_linecount.Text = "200 Lines";
            // 
            // page_messages
            // 
            this.page_messages.Controls.Add(this.MESS_content);
            this.page_messages.Controls.Add(this.groupBox4);
            this.page_messages.Location = new System.Drawing.Point(4, 25);
            this.page_messages.Name = "page_messages";
            this.page_messages.Size = new System.Drawing.Size(632, 418);
            this.page_messages.TabIndex = 4;
            this.page_messages.Text = "Messages";
            this.page_messages.UseVisualStyleBackColor = true;
            // 
            // MESS_content
            // 
            this.MESS_content.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MESS_content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MESS_content.EolMode = ScintillaNET.Eol.Lf;
            this.MESS_content.ExtraAscent = 4;
            this.MESS_content.HScrollBar = false;
            this.MESS_content.Location = new System.Drawing.Point(0, 73);
            this.MESS_content.Margins.Left = 8;
            this.MESS_content.Margins.Right = 8;
            this.MESS_content.Name = "MESS_content";
            this.MESS_content.Size = new System.Drawing.Size(632, 345);
            this.MESS_content.TabIndex = 3;
            this.MESS_content.Text = "NO MESSAGE";
            this.MESS_content.WrapMode = ScintillaNET.WrapMode.Char;
            this.MESS_content.Zoom = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.MESS_NUMBER);
            this.groupBox4.Controls.Add(this.MESS_combobox);
            this.groupBox4.Controls.Add(this.MESS_add);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(632, 73);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "MESS ARCHIVE";
            // 
            // MESS_NUMBER
            // 
            this.MESS_NUMBER.AutoSize = true;
            this.MESS_NUMBER.Location = new System.Drawing.Point(8, 29);
            this.MESS_NUMBER.Name = "MESS_NUMBER";
            this.MESS_NUMBER.Size = new System.Drawing.Size(76, 16);
            this.MESS_NUMBER.TabIndex = 3;
            this.MESS_NUMBER.Text = "0 Messages";
            // 
            // MESS_combobox
            // 
            this.MESS_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MESS_combobox.FormattingEnabled = true;
            this.MESS_combobox.Location = new System.Drawing.Point(112, 25);
            this.MESS_combobox.Name = "MESS_combobox";
            this.MESS_combobox.Size = new System.Drawing.Size(272, 24);
            this.MESS_combobox.TabIndex = 2;
            this.MESS_combobox.SelectedIndexChanged += new System.EventHandler(this.MESS_combobox_SelectedIndexChanged);
            // 
            // MESS_add
            // 
            this.MESS_add.Image = global::FateForge.Properties.Resources.icon_plus;
            this.MESS_add.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MESS_add.Location = new System.Drawing.Point(390, 24);
            this.MESS_add.Name = "MESS_add";
            this.MESS_add.Size = new System.Drawing.Size(81, 26);
            this.MESS_add.TabIndex = 1;
            this.MESS_add.Text = "Add";
            this.MESS_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MESS_add.UseVisualStyleBackColor = true;
            // 
            // Chapter_Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chapter_tabs);
            this.Name = "Chapter_Editor";
            this.Size = new System.Drawing.Size(640, 447);
            this.chapter_tabs.ResumeLayout(false);
            this.page_terrain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_heal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_avo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_def)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terrain_c3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.map_border_y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_border_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_size_y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.map_size_x)).EndInit();
            this.page_spawns.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.page_scripts.ResumeLayout(false);
            this.page_scripts.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.page_messages.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl chapter_tabs;
        private System.Windows.Forms.TabPage page_terrain;
        private System.Windows.Forms.TabPage page_spawns;
        private System.Windows.Forms.TabPage page_persons;
        private System.Windows.Forms.TabPage page_scripts;
        private System.Windows.Forms.TabPage page_messages;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox tile_dragonvein;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox terrain_tiles;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox map_model;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown map_border_y;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown map_border_x;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown map_size_y;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown map_size_x;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox terrain_effect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown terrain_heal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown terrain_avo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown terrain_def;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown terrain_c1;
        private System.Windows.Forms.NumericUpDown terrain_c2;
        private System.Windows.Forms.NumericUpDown terrain_c3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox terrain_mtid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox terrain_tid;
        private Ted.TileEditor.TileEditor terrain_editor;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem chapterScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terrainScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel script_linecount;
        private ScintillaNET.Scintilla SCRIPT;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private ScintillaNET.Scintilla MESS_content;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label MESS_NUMBER;
        private System.Windows.Forms.ComboBox MESS_combobox;
        private System.Windows.Forms.Button MESS_add;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TreeView spawn_tree;
        private System.Windows.Forms.TabPage tabPage6;
        private Dispos_Panel dispos_character;
    }
}
