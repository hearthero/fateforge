﻿namespace FateForge.UI.Controls
{
    partial class Dispos_Panel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CORE_LEVEL = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.CORE_TEAM = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CORE_Y2 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.CORE_X2 = new System.Windows.Forms.NumericUpDown();
            this.CORE_PID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CORE_Y1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CORE_X1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AI_SPAWN_4 = new System.Windows.Forms.NumericUpDown();
            this.AI_SPAWN_3 = new System.Windows.Forms.NumericUpDown();
            this.AI_SPAWN_2 = new System.Windows.Forms.NumericUpDown();
            this.AI_SPAWN_1 = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.AI_AC_FLAG = new System.Windows.Forms.TextBox();
            this.AI_AC_PARAMETER = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.AI_MI_PARAMETER = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.AI_MI_FLAG = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.AI_AT_PARAMETER = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.AI_AT_FLAG = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.AI_MV_PARAMETER = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.AI_MV_FLAG = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ITEM_IID_1 = new System.Windows.Forms.TextBox();
            this.ITEM_1_BIT_4 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_1_BIT_3 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_1_BIT_2 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_1_BIT_1 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_2_BIT_4 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_2_BIT_3 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_2_BIT_2 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_2_BIT_1 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_IID_2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ITEM_3_BIT_4 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_3_BIT_3 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_3_BIT_2 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_3_BIT_1 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_IID_3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.ITEM_4_BIT_4 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_4_BIT_3 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_4_BIT_2 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_4_BIT_1 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_IID_4 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.ITEM_5_BIT_4 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_5_BIT_3 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_5_BIT_2 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_5_BIT_1 = new System.Windows.Forms.NumericUpDown();
            this.ITEM_IID_5 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.MSEID_5 = new System.Windows.Forms.TextBox();
            this.MSEID_4 = new System.Windows.Forms.TextBox();
            this.MSEID_3 = new System.Windows.Forms.TextBox();
            this.MSEID_2 = new System.Windows.Forms.TextBox();
            this.MSEID_1 = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_LEVEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_TEAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_Y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_X2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_Y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_X1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(200, 791);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(192, 762);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(186, 754);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CORE_LEVEL);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.CORE_TEAM);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.CORE_Y2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.CORE_X2);
            this.groupBox1.Controls.Add(this.CORE_PID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.CORE_Y1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.CORE_X1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 3, 9, 3);
            this.groupBox1.Size = new System.Drawing.Size(180, 274);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Core Settings";
            // 
            // CORE_LEVEL
            // 
            this.CORE_LEVEL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CORE_LEVEL.Location = new System.Drawing.Point(12, 137);
            this.CORE_LEVEL.Name = "CORE_LEVEL";
            this.CORE_LEVEL.Size = new System.Drawing.Size(158, 26);
            this.CORE_LEVEL.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Level";
            // 
            // CORE_TEAM
            // 
            this.CORE_TEAM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CORE_TEAM.Location = new System.Drawing.Point(12, 89);
            this.CORE_TEAM.Name = "CORE_TEAM";
            this.CORE_TEAM.Size = new System.Drawing.Size(158, 26);
            this.CORE_TEAM.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Team";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Y2";
            // 
            // CORE_Y2
            // 
            this.CORE_Y2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CORE_Y2.Location = new System.Drawing.Point(100, 236);
            this.CORE_Y2.Name = "CORE_Y2";
            this.CORE_Y2.Size = new System.Drawing.Size(70, 26);
            this.CORE_Y2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "X2";
            // 
            // CORE_X2
            // 
            this.CORE_X2.Location = new System.Drawing.Point(12, 236);
            this.CORE_X2.Name = "CORE_X2";
            this.CORE_X2.Size = new System.Drawing.Size(70, 26);
            this.CORE_X2.TabIndex = 7;
            // 
            // CORE_PID
            // 
            this.CORE_PID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CORE_PID.Location = new System.Drawing.Point(11, 41);
            this.CORE_PID.Name = "CORE_PID";
            this.CORE_PID.Size = new System.Drawing.Size(158, 26);
            this.CORE_PID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Y";
            // 
            // CORE_Y1
            // 
            this.CORE_Y1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CORE_Y1.Location = new System.Drawing.Point(100, 188);
            this.CORE_Y1.Name = "CORE_Y1";
            this.CORE_Y1.Size = new System.Drawing.Size(70, 26);
            this.CORE_Y1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "PID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "X";
            // 
            // CORE_X1
            // 
            this.CORE_X1.Location = new System.Drawing.Point(12, 188);
            this.CORE_X1.Name = "CORE_X1";
            this.CORE_X1.Size = new System.Drawing.Size(70, 26);
            this.CORE_X1.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AI_MV_PARAMETER);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.AI_MV_FLAG);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.AI_AT_PARAMETER);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.AI_AT_FLAG);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.AI_MI_PARAMETER);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.AI_MI_FLAG);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.AI_AC_PARAMETER);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.AI_AC_FLAG);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.AI_SPAWN_4);
            this.groupBox2.Controls.Add(this.AI_SPAWN_3);
            this.groupBox2.Controls.Add(this.AI_SPAWN_2);
            this.groupBox2.Controls.Add(this.AI_SPAWN_1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 3, 9, 3);
            this.groupBox2.Size = new System.Drawing.Size(180, 468);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AI Settings";
            // 
            // AI_SPAWN_4
            // 
            this.AI_SPAWN_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_SPAWN_4.Hexadecimal = true;
            this.AI_SPAWN_4.Location = new System.Drawing.Point(133, 50);
            this.AI_SPAWN_4.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.AI_SPAWN_4.Name = "AI_SPAWN_4";
            this.AI_SPAWN_4.Size = new System.Drawing.Size(36, 26);
            this.AI_SPAWN_4.TabIndex = 4;
            // 
            // AI_SPAWN_3
            // 
            this.AI_SPAWN_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_SPAWN_3.Hexadecimal = true;
            this.AI_SPAWN_3.Location = new System.Drawing.Point(93, 50);
            this.AI_SPAWN_3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.AI_SPAWN_3.Name = "AI_SPAWN_3";
            this.AI_SPAWN_3.Size = new System.Drawing.Size(36, 26);
            this.AI_SPAWN_3.TabIndex = 3;
            // 
            // AI_SPAWN_2
            // 
            this.AI_SPAWN_2.Hexadecimal = true;
            this.AI_SPAWN_2.Location = new System.Drawing.Point(53, 50);
            this.AI_SPAWN_2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.AI_SPAWN_2.Name = "AI_SPAWN_2";
            this.AI_SPAWN_2.Size = new System.Drawing.Size(36, 26);
            this.AI_SPAWN_2.TabIndex = 2;
            // 
            // AI_SPAWN_1
            // 
            this.AI_SPAWN_1.Hexadecimal = true;
            this.AI_SPAWN_1.Location = new System.Drawing.Point(12, 50);
            this.AI_SPAWN_1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.AI_SPAWN_1.Name = "AI_SPAWN_1";
            this.AI_SPAWN_1.Size = new System.Drawing.Size(36, 26);
            this.AI_SPAWN_1.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Spawn Flags";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ITEM_5_BIT_4);
            this.tabPage3.Controls.Add(this.ITEM_5_BIT_3);
            this.tabPage3.Controls.Add(this.ITEM_5_BIT_2);
            this.tabPage3.Controls.Add(this.ITEM_5_BIT_1);
            this.tabPage3.Controls.Add(this.ITEM_IID_5);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.pictureBox5);
            this.tabPage3.Controls.Add(this.ITEM_4_BIT_4);
            this.tabPage3.Controls.Add(this.ITEM_4_BIT_3);
            this.tabPage3.Controls.Add(this.ITEM_4_BIT_2);
            this.tabPage3.Controls.Add(this.ITEM_4_BIT_1);
            this.tabPage3.Controls.Add(this.ITEM_IID_4);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.pictureBox4);
            this.tabPage3.Controls.Add(this.ITEM_3_BIT_4);
            this.tabPage3.Controls.Add(this.ITEM_3_BIT_3);
            this.tabPage3.Controls.Add(this.ITEM_3_BIT_2);
            this.tabPage3.Controls.Add(this.ITEM_3_BIT_1);
            this.tabPage3.Controls.Add(this.ITEM_IID_3);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.pictureBox3);
            this.tabPage3.Controls.Add(this.ITEM_2_BIT_4);
            this.tabPage3.Controls.Add(this.ITEM_2_BIT_3);
            this.tabPage3.Controls.Add(this.ITEM_2_BIT_2);
            this.tabPage3.Controls.Add(this.ITEM_2_BIT_1);
            this.tabPage3.Controls.Add(this.ITEM_IID_2);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.pictureBox2);
            this.tabPage3.Controls.Add(this.ITEM_1_BIT_4);
            this.tabPage3.Controls.Add(this.ITEM_1_BIT_3);
            this.tabPage3.Controls.Add(this.ITEM_1_BIT_2);
            this.tabPage3.Controls.Add(this.ITEM_1_BIT_1);
            this.tabPage3.Controls.Add(this.ITEM_IID_1);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(192, 762);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Items";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.pictureBox10);
            this.tabPage4.Controls.Add(this.pictureBox9);
            this.tabPage4.Controls.Add(this.pictureBox8);
            this.tabPage4.Controls.Add(this.pictureBox7);
            this.tabPage4.Controls.Add(this.pictureBox6);
            this.tabPage4.Controls.Add(this.MSEID_5);
            this.tabPage4.Controls.Add(this.MSEID_4);
            this.tabPage4.Controls.Add(this.MSEID_3);
            this.tabPage4.Controls.Add(this.MSEID_2);
            this.tabPage4.Controls.Add(this.MSEID_1);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(192, 762);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Skills";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 5;
            this.label9.Text = "AC Flag";
            // 
            // AI_AC_FLAG
            // 
            this.AI_AC_FLAG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_AC_FLAG.Location = new System.Drawing.Point(12, 98);
            this.AI_AC_FLAG.Name = "AI_AC_FLAG";
            this.AI_AC_FLAG.Size = new System.Drawing.Size(157, 26);
            this.AI_AC_FLAG.TabIndex = 6;
            // 
            // AI_AC_PARAMETER
            // 
            this.AI_AC_PARAMETER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_AC_PARAMETER.Location = new System.Drawing.Point(12, 146);
            this.AI_AC_PARAMETER.Name = "AI_AC_PARAMETER";
            this.AI_AC_PARAMETER.Size = new System.Drawing.Size(157, 26);
            this.AI_AC_PARAMETER.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "AC Parameter";
            // 
            // AI_MI_PARAMETER
            // 
            this.AI_MI_PARAMETER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_MI_PARAMETER.Location = new System.Drawing.Point(11, 242);
            this.AI_MI_PARAMETER.Name = "AI_MI_PARAMETER";
            this.AI_MI_PARAMETER.Size = new System.Drawing.Size(157, 26);
            this.AI_MI_PARAMETER.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 16);
            this.label11.TabIndex = 11;
            this.label11.Text = "MI Parameter";
            // 
            // AI_MI_FLAG
            // 
            this.AI_MI_FLAG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_MI_FLAG.Location = new System.Drawing.Point(11, 194);
            this.AI_MI_FLAG.Name = "AI_MI_FLAG";
            this.AI_MI_FLAG.Size = new System.Drawing.Size(157, 26);
            this.AI_MI_FLAG.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 16);
            this.label12.TabIndex = 9;
            this.label12.Text = "MI Flag";
            // 
            // AI_AT_PARAMETER
            // 
            this.AI_AT_PARAMETER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_AT_PARAMETER.Location = new System.Drawing.Point(11, 338);
            this.AI_AT_PARAMETER.Name = "AI_AT_PARAMETER";
            this.AI_AT_PARAMETER.Size = new System.Drawing.Size(157, 26);
            this.AI_AT_PARAMETER.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 319);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 16);
            this.label13.TabIndex = 15;
            this.label13.Text = "AT Parameter";
            // 
            // AI_AT_FLAG
            // 
            this.AI_AT_FLAG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_AT_FLAG.Location = new System.Drawing.Point(11, 290);
            this.AI_AT_FLAG.Name = "AI_AT_FLAG";
            this.AI_AT_FLAG.Size = new System.Drawing.Size(157, 26);
            this.AI_AT_FLAG.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 271);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 16);
            this.label14.TabIndex = 13;
            this.label14.Text = "AT Flag";
            // 
            // AI_MV_PARAMETER
            // 
            this.AI_MV_PARAMETER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_MV_PARAMETER.Location = new System.Drawing.Point(12, 434);
            this.AI_MV_PARAMETER.Name = "AI_MV_PARAMETER";
            this.AI_MV_PARAMETER.Size = new System.Drawing.Size(157, 26);
            this.AI_MV_PARAMETER.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 415);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 16);
            this.label15.TabIndex = 19;
            this.label15.Text = "MV Parameter";
            // 
            // AI_MV_FLAG
            // 
            this.AI_MV_FLAG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AI_MV_FLAG.Location = new System.Drawing.Point(12, 386);
            this.AI_MV_FLAG.Name = "AI_MV_FLAG";
            this.AI_MV_FLAG.Size = new System.Drawing.Size(157, 26);
            this.AI_MV_FLAG.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 367);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 16);
            this.label16.TabIndex = 17;
            this.label16.Text = "MV Flag";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(16, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(38, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 16);
            this.label17.TabIndex = 1;
            this.label17.Text = "Item 1";
            // 
            // ITEM_IID_1
            // 
            this.ITEM_IID_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_IID_1.Location = new System.Drawing.Point(16, 35);
            this.ITEM_IID_1.Name = "ITEM_IID_1";
            this.ITEM_IID_1.Size = new System.Drawing.Size(163, 26);
            this.ITEM_IID_1.TabIndex = 2;
            // 
            // ITEM_1_BIT_4
            // 
            this.ITEM_1_BIT_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_1_BIT_4.Hexadecimal = true;
            this.ITEM_1_BIT_4.Location = new System.Drawing.Point(143, 67);
            this.ITEM_1_BIT_4.Name = "ITEM_1_BIT_4";
            this.ITEM_1_BIT_4.Size = new System.Drawing.Size(36, 26);
            this.ITEM_1_BIT_4.TabIndex = 8;
            // 
            // ITEM_1_BIT_3
            // 
            this.ITEM_1_BIT_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_1_BIT_3.Hexadecimal = true;
            this.ITEM_1_BIT_3.Location = new System.Drawing.Point(101, 67);
            this.ITEM_1_BIT_3.Name = "ITEM_1_BIT_3";
            this.ITEM_1_BIT_3.Size = new System.Drawing.Size(36, 26);
            this.ITEM_1_BIT_3.TabIndex = 7;
            // 
            // ITEM_1_BIT_2
            // 
            this.ITEM_1_BIT_2.Hexadecimal = true;
            this.ITEM_1_BIT_2.Location = new System.Drawing.Point(59, 67);
            this.ITEM_1_BIT_2.Name = "ITEM_1_BIT_2";
            this.ITEM_1_BIT_2.Size = new System.Drawing.Size(36, 26);
            this.ITEM_1_BIT_2.TabIndex = 6;
            // 
            // ITEM_1_BIT_1
            // 
            this.ITEM_1_BIT_1.Hexadecimal = true;
            this.ITEM_1_BIT_1.Location = new System.Drawing.Point(16, 67);
            this.ITEM_1_BIT_1.Name = "ITEM_1_BIT_1";
            this.ITEM_1_BIT_1.Size = new System.Drawing.Size(36, 26);
            this.ITEM_1_BIT_1.TabIndex = 5;
            // 
            // ITEM_2_BIT_4
            // 
            this.ITEM_2_BIT_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_2_BIT_4.Hexadecimal = true;
            this.ITEM_2_BIT_4.Location = new System.Drawing.Point(143, 157);
            this.ITEM_2_BIT_4.Name = "ITEM_2_BIT_4";
            this.ITEM_2_BIT_4.Size = new System.Drawing.Size(36, 26);
            this.ITEM_2_BIT_4.TabIndex = 15;
            // 
            // ITEM_2_BIT_3
            // 
            this.ITEM_2_BIT_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_2_BIT_3.Hexadecimal = true;
            this.ITEM_2_BIT_3.Location = new System.Drawing.Point(101, 157);
            this.ITEM_2_BIT_3.Name = "ITEM_2_BIT_3";
            this.ITEM_2_BIT_3.Size = new System.Drawing.Size(36, 26);
            this.ITEM_2_BIT_3.TabIndex = 14;
            // 
            // ITEM_2_BIT_2
            // 
            this.ITEM_2_BIT_2.Hexadecimal = true;
            this.ITEM_2_BIT_2.Location = new System.Drawing.Point(59, 157);
            this.ITEM_2_BIT_2.Name = "ITEM_2_BIT_2";
            this.ITEM_2_BIT_2.Size = new System.Drawing.Size(36, 26);
            this.ITEM_2_BIT_2.TabIndex = 13;
            // 
            // ITEM_2_BIT_1
            // 
            this.ITEM_2_BIT_1.Hexadecimal = true;
            this.ITEM_2_BIT_1.Location = new System.Drawing.Point(16, 157);
            this.ITEM_2_BIT_1.Name = "ITEM_2_BIT_1";
            this.ITEM_2_BIT_1.Size = new System.Drawing.Size(36, 26);
            this.ITEM_2_BIT_1.TabIndex = 12;
            // 
            // ITEM_IID_2
            // 
            this.ITEM_IID_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_IID_2.Location = new System.Drawing.Point(16, 125);
            this.ITEM_IID_2.Name = "ITEM_IID_2";
            this.ITEM_IID_2.Size = new System.Drawing.Size(163, 26);
            this.ITEM_IID_2.TabIndex = 11;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(38, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 16);
            this.label18.TabIndex = 10;
            this.label18.Text = "Item 2";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(16, 103);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // ITEM_3_BIT_4
            // 
            this.ITEM_3_BIT_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_3_BIT_4.Hexadecimal = true;
            this.ITEM_3_BIT_4.Location = new System.Drawing.Point(143, 249);
            this.ITEM_3_BIT_4.Name = "ITEM_3_BIT_4";
            this.ITEM_3_BIT_4.Size = new System.Drawing.Size(36, 26);
            this.ITEM_3_BIT_4.TabIndex = 22;
            // 
            // ITEM_3_BIT_3
            // 
            this.ITEM_3_BIT_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_3_BIT_3.Hexadecimal = true;
            this.ITEM_3_BIT_3.Location = new System.Drawing.Point(101, 249);
            this.ITEM_3_BIT_3.Name = "ITEM_3_BIT_3";
            this.ITEM_3_BIT_3.Size = new System.Drawing.Size(36, 26);
            this.ITEM_3_BIT_3.TabIndex = 21;
            // 
            // ITEM_3_BIT_2
            // 
            this.ITEM_3_BIT_2.Hexadecimal = true;
            this.ITEM_3_BIT_2.Location = new System.Drawing.Point(59, 249);
            this.ITEM_3_BIT_2.Name = "ITEM_3_BIT_2";
            this.ITEM_3_BIT_2.Size = new System.Drawing.Size(36, 26);
            this.ITEM_3_BIT_2.TabIndex = 20;
            // 
            // ITEM_3_BIT_1
            // 
            this.ITEM_3_BIT_1.Hexadecimal = true;
            this.ITEM_3_BIT_1.Location = new System.Drawing.Point(16, 249);
            this.ITEM_3_BIT_1.Name = "ITEM_3_BIT_1";
            this.ITEM_3_BIT_1.Size = new System.Drawing.Size(36, 26);
            this.ITEM_3_BIT_1.TabIndex = 19;
            // 
            // ITEM_IID_3
            // 
            this.ITEM_IID_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_IID_3.Location = new System.Drawing.Point(16, 217);
            this.ITEM_IID_3.Name = "ITEM_IID_3";
            this.ITEM_IID_3.Size = new System.Drawing.Size(163, 26);
            this.ITEM_IID_3.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(38, 195);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 16);
            this.label19.TabIndex = 17;
            this.label19.Text = "Item 3";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(16, 195);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // ITEM_4_BIT_4
            // 
            this.ITEM_4_BIT_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_4_BIT_4.Hexadecimal = true;
            this.ITEM_4_BIT_4.Location = new System.Drawing.Point(143, 341);
            this.ITEM_4_BIT_4.Name = "ITEM_4_BIT_4";
            this.ITEM_4_BIT_4.Size = new System.Drawing.Size(36, 26);
            this.ITEM_4_BIT_4.TabIndex = 29;
            // 
            // ITEM_4_BIT_3
            // 
            this.ITEM_4_BIT_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_4_BIT_3.Hexadecimal = true;
            this.ITEM_4_BIT_3.Location = new System.Drawing.Point(101, 341);
            this.ITEM_4_BIT_3.Name = "ITEM_4_BIT_3";
            this.ITEM_4_BIT_3.Size = new System.Drawing.Size(36, 26);
            this.ITEM_4_BIT_3.TabIndex = 28;
            // 
            // ITEM_4_BIT_2
            // 
            this.ITEM_4_BIT_2.Hexadecimal = true;
            this.ITEM_4_BIT_2.Location = new System.Drawing.Point(59, 341);
            this.ITEM_4_BIT_2.Name = "ITEM_4_BIT_2";
            this.ITEM_4_BIT_2.Size = new System.Drawing.Size(36, 26);
            this.ITEM_4_BIT_2.TabIndex = 27;
            // 
            // ITEM_4_BIT_1
            // 
            this.ITEM_4_BIT_1.Hexadecimal = true;
            this.ITEM_4_BIT_1.Location = new System.Drawing.Point(16, 341);
            this.ITEM_4_BIT_1.Name = "ITEM_4_BIT_1";
            this.ITEM_4_BIT_1.Size = new System.Drawing.Size(36, 26);
            this.ITEM_4_BIT_1.TabIndex = 26;
            // 
            // ITEM_IID_4
            // 
            this.ITEM_IID_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_IID_4.Location = new System.Drawing.Point(16, 309);
            this.ITEM_IID_4.Name = "ITEM_IID_4";
            this.ITEM_IID_4.Size = new System.Drawing.Size(163, 26);
            this.ITEM_IID_4.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(38, 287);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 16);
            this.label20.TabIndex = 24;
            this.label20.Text = "Item 4";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(16, 287);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.TabIndex = 23;
            this.pictureBox4.TabStop = false;
            // 
            // ITEM_5_BIT_4
            // 
            this.ITEM_5_BIT_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_5_BIT_4.Hexadecimal = true;
            this.ITEM_5_BIT_4.Location = new System.Drawing.Point(143, 436);
            this.ITEM_5_BIT_4.Name = "ITEM_5_BIT_4";
            this.ITEM_5_BIT_4.Size = new System.Drawing.Size(36, 26);
            this.ITEM_5_BIT_4.TabIndex = 36;
            // 
            // ITEM_5_BIT_3
            // 
            this.ITEM_5_BIT_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_5_BIT_3.Hexadecimal = true;
            this.ITEM_5_BIT_3.Location = new System.Drawing.Point(101, 436);
            this.ITEM_5_BIT_3.Name = "ITEM_5_BIT_3";
            this.ITEM_5_BIT_3.Size = new System.Drawing.Size(36, 26);
            this.ITEM_5_BIT_3.TabIndex = 35;
            // 
            // ITEM_5_BIT_2
            // 
            this.ITEM_5_BIT_2.Hexadecimal = true;
            this.ITEM_5_BIT_2.Location = new System.Drawing.Point(59, 436);
            this.ITEM_5_BIT_2.Name = "ITEM_5_BIT_2";
            this.ITEM_5_BIT_2.Size = new System.Drawing.Size(36, 26);
            this.ITEM_5_BIT_2.TabIndex = 34;
            // 
            // ITEM_5_BIT_1
            // 
            this.ITEM_5_BIT_1.Hexadecimal = true;
            this.ITEM_5_BIT_1.Location = new System.Drawing.Point(16, 436);
            this.ITEM_5_BIT_1.Name = "ITEM_5_BIT_1";
            this.ITEM_5_BIT_1.Size = new System.Drawing.Size(36, 26);
            this.ITEM_5_BIT_1.TabIndex = 33;
            // 
            // ITEM_IID_5
            // 
            this.ITEM_IID_5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ITEM_IID_5.Location = new System.Drawing.Point(16, 404);
            this.ITEM_IID_5.Name = "ITEM_IID_5";
            this.ITEM_IID_5.Size = new System.Drawing.Size(163, 26);
            this.ITEM_IID_5.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(38, 382);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 16);
            this.label21.TabIndex = 31;
            this.label21.Text = "Item 5";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(16, 382);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.TabIndex = 30;
            this.pictureBox5.TabStop = false;
            // 
            // MSEID_5
            // 
            this.MSEID_5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MSEID_5.Location = new System.Drawing.Point(46, 227);
            this.MSEID_5.Name = "MSEID_5";
            this.MSEID_5.Size = new System.Drawing.Size(133, 26);
            this.MSEID_5.TabIndex = 37;
            // 
            // MSEID_4
            // 
            this.MSEID_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MSEID_4.Location = new System.Drawing.Point(46, 179);
            this.MSEID_4.Name = "MSEID_4";
            this.MSEID_4.Size = new System.Drawing.Size(133, 26);
            this.MSEID_4.TabIndex = 36;
            // 
            // MSEID_3
            // 
            this.MSEID_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MSEID_3.Location = new System.Drawing.Point(46, 131);
            this.MSEID_3.Name = "MSEID_3";
            this.MSEID_3.Size = new System.Drawing.Size(133, 26);
            this.MSEID_3.TabIndex = 35;
            // 
            // MSEID_2
            // 
            this.MSEID_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MSEID_2.Location = new System.Drawing.Point(46, 83);
            this.MSEID_2.Name = "MSEID_2";
            this.MSEID_2.Size = new System.Drawing.Size(133, 26);
            this.MSEID_2.TabIndex = 34;
            // 
            // MSEID_1
            // 
            this.MSEID_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MSEID_1.Location = new System.Drawing.Point(46, 35);
            this.MSEID_1.Name = "MSEID_1";
            this.MSEID_1.Size = new System.Drawing.Size(133, 26);
            this.MSEID_1.TabIndex = 33;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(16, 36);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(24, 24);
            this.pictureBox6.TabIndex = 38;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(16, 84);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(24, 24);
            this.pictureBox7.TabIndex = 39;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new System.Drawing.Point(16, 132);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(24, 24);
            this.pictureBox8.TabIndex = 40;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new System.Drawing.Point(16, 180);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(24, 24);
            this.pictureBox9.TabIndex = 41;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new System.Drawing.Point(16, 228);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(24, 24);
            this.pictureBox10.TabIndex = 42;
            this.pictureBox10.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(43, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 16);
            this.label22.TabIndex = 43;
            this.label22.Text = "Skill 1";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(43, 64);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 44;
            this.label23.Text = "Skill 2";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(43, 112);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 16);
            this.label24.TabIndex = 45;
            this.label24.Text = "Skill 3";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(43, 160);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 16);
            this.label25.TabIndex = 46;
            this.label25.Text = "Skill 4";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(43, 208);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 16);
            this.label26.TabIndex = 47;
            this.label26.Text = "Skill 5";
            // 
            // Dispos_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Dispos_Panel";
            this.Size = new System.Drawing.Size(200, 791);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_LEVEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_TEAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_Y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_X2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_Y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CORE_X1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AI_SPAWN_1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_1_BIT_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_2_BIT_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_3_BIT_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_4_BIT_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_5_BIT_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ITEM_5_BIT_4;
        private System.Windows.Forms.NumericUpDown ITEM_5_BIT_3;
        private System.Windows.Forms.NumericUpDown ITEM_5_BIT_2;
        private System.Windows.Forms.NumericUpDown ITEM_5_BIT_1;
        private System.Windows.Forms.TextBox ITEM_IID_5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.NumericUpDown ITEM_4_BIT_4;
        private System.Windows.Forms.NumericUpDown ITEM_4_BIT_3;
        private System.Windows.Forms.NumericUpDown ITEM_4_BIT_2;
        private System.Windows.Forms.NumericUpDown ITEM_4_BIT_1;
        private System.Windows.Forms.TextBox ITEM_IID_4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.NumericUpDown ITEM_3_BIT_4;
        private System.Windows.Forms.NumericUpDown ITEM_3_BIT_3;
        private System.Windows.Forms.NumericUpDown ITEM_3_BIT_2;
        private System.Windows.Forms.NumericUpDown ITEM_3_BIT_1;
        private System.Windows.Forms.TextBox ITEM_IID_3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.NumericUpDown ITEM_2_BIT_4;
        private System.Windows.Forms.NumericUpDown ITEM_2_BIT_3;
        private System.Windows.Forms.NumericUpDown ITEM_2_BIT_2;
        private System.Windows.Forms.NumericUpDown ITEM_2_BIT_1;
        private System.Windows.Forms.TextBox ITEM_IID_2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown ITEM_1_BIT_4;
        private System.Windows.Forms.NumericUpDown ITEM_1_BIT_3;
        private System.Windows.Forms.NumericUpDown ITEM_1_BIT_2;
        private System.Windows.Forms.NumericUpDown ITEM_1_BIT_1;
        private System.Windows.Forms.TextBox ITEM_IID_1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox MSEID_5;
        private System.Windows.Forms.TextBox MSEID_4;
        private System.Windows.Forms.TextBox MSEID_3;
        private System.Windows.Forms.TextBox MSEID_2;
        private System.Windows.Forms.TextBox MSEID_1;
        private System.Windows.Forms.NumericUpDown CORE_LEVEL;
        private System.Windows.Forms.NumericUpDown CORE_TEAM;
        private System.Windows.Forms.NumericUpDown CORE_Y2;
        private System.Windows.Forms.NumericUpDown CORE_X2;
        private System.Windows.Forms.TextBox CORE_PID;
        private System.Windows.Forms.NumericUpDown CORE_Y1;
        private System.Windows.Forms.NumericUpDown CORE_X1;
        private System.Windows.Forms.NumericUpDown AI_SPAWN_4;
        private System.Windows.Forms.NumericUpDown AI_SPAWN_3;
        private System.Windows.Forms.NumericUpDown AI_SPAWN_2;
        private System.Windows.Forms.NumericUpDown AI_SPAWN_1;
        private System.Windows.Forms.TextBox AI_MV_PARAMETER;
        private System.Windows.Forms.TextBox AI_MV_FLAG;
        private System.Windows.Forms.TextBox AI_AT_PARAMETER;
        private System.Windows.Forms.TextBox AI_AT_FLAG;
        private System.Windows.Forms.TextBox AI_MI_PARAMETER;
        private System.Windows.Forms.TextBox AI_MI_FLAG;
        private System.Windows.Forms.TextBox AI_AC_PARAMETER;
        private System.Windows.Forms.TextBox AI_AC_FLAG;
    }
}
