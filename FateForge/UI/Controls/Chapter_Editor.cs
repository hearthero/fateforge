﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using FEFLib.gamedata.terrain;
using Ted.TileEditor;
using FEFLib.gamedata.dispos;
using ScintillaNET;

namespace FateForge.UI.Controls
{
    public partial class Chapter_Editor : UserControl
    {

        private FatesTerrainFile terrain;
        private FileStream terrain_file;

        private FatesDispos dispos;

        public static Dictionary<string, string> MESS_current = new Dictionary<string, string>();

        public Chapter_Editor()
        {
            InitializeComponent();

            SCRIPT.StyleResetDefault();
            SCRIPT.Styles[Style.Default].Font = "Consolas";
            SCRIPT.Styles[Style.Default].Size = 10;

            SCRIPT.Margins[0].Width = 32;

            SCRIPT.Styles[Style.Default].BackColor = Color.FromArgb(30, 30, 30);
            SCRIPT.Styles[Style.Default].ForeColor = Color.FromArgb(220, 220, 220);
            SCRIPT.StyleClearAll();

            SCRIPT.Styles[Style.LineNumber].ForeColor = Color.FromArgb(87, 166, 74);
            SCRIPT.Styles[Style.LineNumber].BackColor = Color.FromArgb(30, 30, 30);

            SCRIPT.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(0, 128, 0); // Green
            SCRIPT.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(0, 128, 0); // Green
            SCRIPT.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(128, 128, 128); // Gray

            SCRIPT.Styles[Style.Cpp.Number].ForeColor = Color.FromArgb(181, 206, 168);
            SCRIPT.Styles[Style.Cpp.Word].ForeColor = Color.FromArgb(78, 201, 176);
            SCRIPT.Styles[Style.Cpp.Word2].ForeColor = Color.FromArgb(86, 156, 214);
            SCRIPT.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(214, 157, 133); // Red
            SCRIPT.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(163, 21, 21); // Red
            SCRIPT.Styles[Style.Cpp.Verbatim].ForeColor = Color.FromArgb(163, 21, 21); // Red
            SCRIPT.Styles[Style.Cpp.StringEol].BackColor = Color.Pink;
            SCRIPT.Styles[Style.Cpp.Operator].ForeColor = Color.FromArgb(220, 220, 220);
            SCRIPT.Styles[Style.Cpp.Preprocessor].ForeColor = Color.Maroon;
            SCRIPT.Lexer = Lexer.Cpp;

            SCRIPT.SetKeywords(0, "Event Subheader");
            SCRIPT.SetKeywords(1, "bool byte raw char class const decimal double enum float int long sbyte short static string struct uint ulong ushort false true void end pass specialCheck fail");

        }

        public void load_data(string node)
        {
            string file = Path.GetFullPath(node).Replace(Globals.Folder_Decrypted, "");
            string MESS_path = Path.Combine(Globals.Folder_Encrypted, "m", "@U", file.Substring(0, file.Length - 7) + ".txt");
            MESS_path = MESS_path.Replace("@U" + Path.DirectorySeparatorChar + "A" + Path.DirectorySeparatorChar, "A" + Path.DirectorySeparatorChar + "@U" + Path.DirectorySeparatorChar);
            MESS_path = MESS_path.Replace("@U" + Path.DirectorySeparatorChar + "B" + Path.DirectorySeparatorChar, "B" + Path.DirectorySeparatorChar + "@U" + Path.DirectorySeparatorChar);
            MESS_path = MESS_path.Replace("@U" + Path.DirectorySeparatorChar + "C" + Path.DirectorySeparatorChar, "C" + Path.DirectorySeparatorChar + "@U" + Path.DirectorySeparatorChar);

            string SCRIPT_path = Path.Combine(Globals.Folder_Encrypted, "Scripts", file.Substring(0, file.Length - 7) + ".fscript");

            string TERRAIN_path = Path.Combine(Globals.Folder_Encrypted, "GameData", "Terrain", file.Substring(0, file.Length - 7) + ".bin");
            TERRAIN_path = TERRAIN_path.Replace("A" + Path.DirectorySeparatorChar, "");
            TERRAIN_path = TERRAIN_path.Replace("B" + Path.DirectorySeparatorChar, "");
            TERRAIN_path = TERRAIN_path.Replace("C" + Path.DirectorySeparatorChar, "");

            string DISPOS_path = Path.Combine(Globals.Folder_Encrypted, "GameData", "Dispos", file.Substring(0, file.Length - 7) + ".bin");

            Console.WriteLine(DISPOS_path);

            MESS_current.Clear();
            MESS_combobox.Items.Clear();
            MESS_content.Text = "NO MESSAGE";
            SCRIPT.Text = "NO SCRIPT";

            try
            {
                if (File.Exists(MESS_path))
                {
                    string GameData = File.ReadAllText(MESS_path);
                    Regex rgx = new Regex(@"^MID_(.*?): (.*?)$", RegexOptions.Multiline);

                    foreach (Match match in rgx.Matches(GameData))
                    {
                        MESS_current.Add(match.Groups[1].Value, match.Groups[2].Value);
                        MESS_combobox.Items.Add(match.Groups[1].Value);
                    }
                    MESS_NUMBER.Text = MESS_current.Count().ToString() + " Messages";
                    MESS_combobox.SelectedIndex = 0;
                }
                else
                {
                    MESS_NUMBER.Text = "No Messages";
                }
                if (File.Exists(SCRIPT_path))
                {
                    SCRIPT.Text = File.ReadAllText(SCRIPT_path);
                }
                if (File.Exists(TERRAIN_path))
                {
                    if (terrain_file != null)
                        terrain_file.Close();
                    LoadTerrainFile(TERRAIN_path);
                }
                if (File.Exists(DISPOS_path))
                {
                    dispos = new FatesDispos(DISPOS_path);

                    spawn_tree.Nodes.Clear();
                    foreach (DisposFaction faction in dispos.getFactions())
                    {
                        TreeNode root = new TreeNode(faction.getName());

                        if (faction.getName() == "Ally")
                            root.ForeColor = Color.Green;

                        if (faction.getName() == "Enemy")
                            root.ForeColor = Color.Red;

                        if (faction.getName().StartsWith("Reinforce"))
                            root.ForeColor = Color.Red;

                        if (faction.getName() == "Player")
                            root.ForeColor = Color.Blue;

                        foreach (DisposBlock block in faction.getSpawns())
                        {
                            TreeNode character = new TreeNode(block.getPid());

                            root.Nodes.Add(character);
                        }

                        spawn_tree.Nodes.Add(root);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private Bitmap GetTileImage(int tile)
        {
            return (Bitmap)Properties.Resources.ResourceManager.GetObject("tile" + tile);
        }

        private void LoadTerrainFile(string path)
        {
            terrain_file = File.Open(path, FileMode.Open);
            terrain = new FatesTerrainFile(terrain_file, Encoding.GetEncoding(932));

            byte[,] map = terrain.getMap();
            map_model.Text = terrain.getMapModel();
            List<FEFLib.gamedata.terrain.TileBlock> tiles = terrain.getTiles();

            terrain_editor.MapSize = new Size(terrain.getMapSizeX(), terrain.getMapSizeY());
            map_size_x.Value = terrain.getMapSizeX();
            map_size_y.Value = terrain.getMapSizeY();

            map_border_x.Value = terrain.getBorderSizeX();
            map_border_y.Value = terrain.getBorderSizeY();

            terrain_tiles.Items.Clear();
            foreach (FEFLib.gamedata.terrain.TileBlock tile in tiles)
            {
                if (Globals.GameData_Terrain.ContainsKey(tile.getmTid()))
                    terrain_tiles.Items.Add(Globals.GameData_Terrain[tile.getmTid()] + " / " + tile.getTid());
                else
                    terrain_tiles.Items.Add("Nothing");
            }

            terrain_tiles.SelectedIndex = 0;

            LoadTerrain();
        }

        private void LoadTerrain()
        {
            byte[,] map = terrain.getMap();
            List<FEFLib.gamedata.terrain.TileBlock> tiles = terrain.getTiles();

            for (int x = 0; x < terrain.getMapSizeY(); x += 1)
            {
                for (int y = 0; y < terrain.getMapSizeX(); y += 1)
                {
                    terrain_editor.SetTile(y, x, 0, 255);
                }
            }

            for (int x = terrain.getBorderSizeX(); x < terrain.getMapSizeY() - terrain.getBorderSizeX(); x += 1)
            {
                for (int y = terrain.getBorderSizeY(); y < terrain.getMapSizeX() - terrain.getBorderSizeY(); y += 1)
                {
                    terrain_editor.SetTile(y, x, 0, tid_to_int(tiles[map[x, y]].getTid()));
                }
            }
        }

        private int tid_to_int(string tid)
        {
            Regex dragonvein_check = new Regex("^TID_(.*?)／(.*$)", RegexOptions.Multiline);
            if (dragonvein_check.Match(tid).Success)
            {
                if (tile_dragonvein.Checked)
                    tid = "TID_" + dragonvein_check.Match(tid).Groups[2].Value;
                else
                    tid = "TID_" + dragonvein_check.Match(tid).Groups[1].Value;
            }

            switch (tid)
            {
                case "TID_平地":
                case "TID_床＠障害物":
                case "TID_平地＃竜脈":
                    return 0;       // Plains
                case "TID_林":
                    return 1;       // Forest
                case "TID_砦":
                case "TID_墟門":
                case "TID_廃墟門":
                case "TID_玉座＃竜脈":
                    return 2;       // Fort
                case "TID_階段":
                case "TID_階段（戦闘可能）":
                case "TID_階段＃竜脈":
                    return 3;       // Stairs
                case "TID_暗器砲台":
                case "TID_弓砲台":
                case "TID_魔道砲台":
                    return 4;       // Warfare
                case "TID_宝箱":
                    return 5;       // Chest
                case "TID_床":
                case "TID_橋":
                case "TID_砂漠":
                case "TID_床＃竜脈":
                    return 6;       // Sligthly darker
                case "TID_壁":
                case "TID_障害物＠床":
                case "TID_古木｜平地":
                    return 7;       // Dark grey
                case "TID_無し":
                case "TID_空":
                    return 8;       // Empty
                case "TID_回復床":
                    return 9;       // Healing
                case "TID_柱":
                    return 10;
                case "TID_荒地":
                    return 11;      // Pillars
                case "TID_低建物":
                case "TID_建物｜平地":
                case "TID_建物":
                    return 12;      // Building
                case "TID_河":
                case "TID_空堀":
                case "TID_海":
                case "TID_河｜橋":
                case "TID_閉じ村":
                case "TID_廃村":
                case "TID_氷海":
                case "TID_湖":
                    return 13;      // Water
                case "TID_瓦礫":
                case "TID_低壁":
                case "TID_山":
                    return 14;      // Soft Orange
                case "TID_扉":
                case "TID_平地｜平地":
                    return 15;      // Door
                case "TID_高山":
                    return 16;
                case "TID_深い森":
                    return 17;
                case "TID_毒沼":
                case "TID_竹槍":
                    return 18;
                default:
                    return 255;     // Transparent
            }
        }

        private void terrain_editor_PaintTile(object sender, Ted.TileEditor.TileEventArgs e)
        {
            int tile = terrain_editor[e.Location];
            Bitmap b = (Bitmap)GetTileImage(tile).Clone();

            if (e.Selected)
            {
                e.PaintArgs.Graphics.DrawImage(TileEditor.InvertBitmap(b),
                    e.PaintArgs.ClipRectangle, new Rectangle(0, 0, 6, 6), GraphicsUnit.Pixel);
            }
            else {
                e.PaintArgs.Graphics.DrawImage(b,
                    e.PaintArgs.ClipRectangle, new Rectangle(0, 0, 6, 6), GraphicsUnit.Pixel);
            }
        }

        private void terrain_editor_MouseDown(object sender, Ted.TileEditor.MouseTileEventArgs e)
        {
            if (e.Selecting || terrain_tiles.SelectedIndex == -1)
                return;
            TilePoint pt = e.Location;
            if (e.MouseArgs.Button == MouseButtons.Left)
            {

            }
            //UserSetTile(pt, LeftTile);
            else if (e.MouseArgs.Button == MouseButtons.Right)
            {
                byte[,] terrain_map = terrain.getMap();
                if (pt.X != -1 && pt.Y != -1)
                    terrain_tiles.SelectedIndex = terrain_map[pt.Y, pt.X];
            }
        }

        private void terrain_tiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            FEFLib.gamedata.terrain.TileBlock current_tile = terrain.getTiles()[terrain_tiles.SelectedIndex];
            terrain_tid.Text = current_tile.getTid();
            if (Globals.GameData_Terrain.ContainsKey(current_tile.getmTid()))
                terrain_mtid.Text = Globals.GameData_Terrain[current_tile.getmTid()];
            else
                terrain_mtid.Text = current_tile.getmTid();

            terrain_def.Value = current_tile.getDefenseBonus();
            terrain_avo.Value = current_tile.getAvoidBonus();
            terrain_heal.Value = current_tile.getHealingBonus();
            terrain_effect.Text = current_tile.getEffect();
            byte[] c_ids = current_tile.getChangeIds();
            terrain_c1.Value = c_ids[0];
            terrain_c2.Value = c_ids[1];
            terrain_c3.Value = c_ids[2];
        }

        private void SCRIPT_TextChanged(object sender, EventArgs e)
        {
            script_linecount.Text = SCRIPT.Lines.Count + " Lines";
        }

        private void MESS_combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            MESS_content.Text = MESS_current[MESS_combobox.Text];
        }

        private void tile_dragonvein_CheckedChanged(object sender, EventArgs e)
        {
            LoadTerrain();
        }

        private void spawn_tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }

        private void spawn_tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (spawn_tree.SelectedNode != null)
            {
                if (spawn_tree.SelectedNode.Parent != null)
                {
                    TreeNode selected = spawn_tree.SelectedNode;
                    DisposBlock active_person = dispos.getFactions()[selected.Parent.Index].getSpawns()[selected.Index];

                    dispos_character.LoadDispos(active_person);

                }
            }
        }
    }
}
