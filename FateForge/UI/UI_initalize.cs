﻿using DSDecmp.Formats.Nitro;
using FEFLib.script;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FateForge
{
    public partial class UI_initalize : Form
    {
        public UI_initalize()
        {
            InitializeComponent();
        }

        public ScriptDecompiler decompiler;

        private byte[] LZ11Decompress(byte[] compressed)
        {
            using (MemoryStream cstream = new MemoryStream(compressed))
            {
                using (MemoryStream dstream = new MemoryStream())
                {
                    (new LZ11()).Decompress(cstream, compressed.Length, dstream);
                    return dstream.ToArray();
                }
            }
        }

        private byte[] LZ11Compress(byte[] decompressed)
        {
            using (MemoryStream dstream = new MemoryStream(decompressed))
            {
                using (MemoryStream cstream = new MemoryStream())
                {
                    (new LZ11()).Compress(dstream, decompressed.Length, cstream);
                    return cstream.ToArray();
                }
            }
        }

        private string ExtractFireEmblemMessageArchive(string outname, byte[] archive)
        {
            var ShiftJIS = Encoding.GetEncoding(932);
            string ArchiveName = ShiftJIS.GetString(archive.Skip(0x20).TakeWhile(b => b != 0).ToArray()); // Archive Name.
            uint TextPartitionLen = BitConverter.ToUInt32(archive, 4);
            uint StringCount = BitConverter.ToUInt32(archive, 0xC);
            string[] MessageNames = new string[StringCount];
            string[] Messages = new string[StringCount];

            uint StringMetaOffset = 0x20 + TextPartitionLen;
            uint NamesOffset = StringMetaOffset + 0x8 * StringCount;

            for (int i = 0; i < StringCount; i++)
            {
                int MessageOffset = 0x20 + BitConverter.ToInt32(archive, (int)StringMetaOffset + 0x8 * i);
                int MessageLen = 0;
                while (BitConverter.ToUInt16(archive, MessageOffset + MessageLen) != 0)
                    MessageLen += 2;
                Messages[i] = Encoding.Unicode.GetString(archive.Skip(MessageOffset).Take(MessageLen).ToArray()).Replace("\n", "\\n").Replace("\r", "\\r");
                int NameOffset = (int)NamesOffset + BitConverter.ToInt32(archive, (int)StringMetaOffset + (0x8 * i) + 4);
                MessageNames[i] = ShiftJIS.GetString(archive.Skip(NameOffset).TakeWhile(b => b != 0).ToArray());
            }

            List<string> Lines = new List<string>
            {
                ArchiveName,
                Environment.NewLine,
                "Message Name: Message",
                Environment.NewLine
            };
            for (int i = 0; i < StringCount; i++)
                Lines.Add(string.Format("{0}: {1}", MessageNames[i], Messages[i]));
            File.WriteAllLines(outname, Lines);

            return ArchiveName;
        }

        private void UI_initalize_Shown(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;

            /*
            foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, "ExtractedRomFS"), "*.*", SearchOption.AllDirectories))
            {
                setup_progress.Maximum++;
            }

            foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, "ExtractedRomFS", "Scripts"), "*.cmb*", SearchOption.AllDirectories))
            {
                setup_progress.Maximum++;
            }

            foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, Globals.Project.Name + "Decrypted"), "*.bin*", SearchOption.AllDirectories))
            {
                setup_progress.Maximum++;
            }

            Text = "Decompressing files";

            foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, "ExtractedRomFS"), "*.*", SearchOption.AllDirectories))
            {
                string path_segment = file.Substring(Path.Combine(Globals.Project.Dir, "ExtractedRomFS").Length);
                string decpath = Path.Combine(Globals.Project.Dir, Globals.Project.Name + "Decrypted") + path_segment.Substring(0, path_segment.Length - 3);
                string ext = Path.GetExtension(file).ToLower();

                Directory.CreateDirectory(Path.GetDirectoryName(decpath));

                // Decompress Files

                byte[] filedata = File.ReadAllBytes(file);

                if (ext == ".lz")
                {   
                    if (filedata[0] == 0x13 && filedata[4] == 0x11) // "LZ13"
                    {
                        filedata = filedata.Skip(4).ToArray();
                    }
                    else if (filedata[0] == 0x17 && filedata[4] == 0x11) // "LZ17"
                    {
                        var xorkey = BitConverter.ToUInt32(filedata, 0) >> 8;
                        xorkey *= 0x8083;
                        for (var i = 8; i < filedata.Length; i += 0x4)
                        {
                            BitConverter.GetBytes(BitConverter.ToUInt32(filedata, i) ^ xorkey).CopyTo(filedata, i);
                            xorkey ^= BitConverter.ToUInt32(filedata, i);
                        }
                        filedata = filedata.Skip(4).ToArray();
                    }
                    else if (filedata[0] == 0x4 && (BitConverter.ToUInt32(filedata, 0) >> 8) == filedata.Length - 4)
                    {
                        var xorkey = BitConverter.ToUInt32(filedata, 0) >> 8;
                        xorkey *= 0x8083;
                        for (var i = 4; i < filedata.Length; i += 0x4)
                        {
                            BitConverter.GetBytes(BitConverter.ToUInt32(filedata, i) ^ xorkey).CopyTo(filedata, i);
                            xorkey ^= BitConverter.ToUInt32(filedata, i);
                        }
                        filedata = filedata.Skip(4).ToArray();
                        if (BitConverter.ToUInt32(filedata, 0) == filedata.Length)
                        {
                            if (!File.Exists(decpath))
                                File.WriteAllBytes(decpath, filedata);
                        }
                        return;
                    }
                    try
                    {
                        if (!File.Exists(decpath))
                            File.WriteAllBytes(decpath, LZ11Decompress(filedata));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                setup_progress.Value++;
                Refresh();
            }

            Text = "Decrypting message archives";

                foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, Globals.Project.Name + "Decrypted"), "*.bin*", SearchOption.AllDirectories))
            {
                var outname = file.Substring(0, file.Length - 4) + ".txt";
                byte[] filedata = File.ReadAllBytes(file);
                if (BitConverter.ToUInt32(filedata, 0) == filedata.Length &&
                    new string(filedata.Skip(0x20).Take(0xC).Select(c => (char)c).ToArray()) == "MESS_ARCHIVE")
                {
                    string archive_name = ExtractFireEmblemMessageArchive(outname, filedata);
                }
                setup_progress.Value++;
                Refresh();
            }
            */
            Text = "Decompiling scripts";

            foreach (string file in Directory.EnumerateFiles(Path.Combine(Globals.Project.Dir, "ExtractedRomFS", "Scripts"), "*.cmb*", SearchOption.AllDirectories))
            {
                string path_segment = file.Substring(Path.Combine(Globals.Project.Dir, "ExtractedRomFS").Length);
                string decpath = Path.Combine(Globals.Project.Dir, Globals.Project.Name + "Decrypted") + path_segment.Substring(0, path_segment.Length - 4) + ".fscript";

                Directory.CreateDirectory(Path.GetDirectoryName(decpath));

                if (!File.Exists(decpath))
                {
                    decompiler = new ScriptDecompiler();
                    string script = decompiler.decompile(file);
                    File.WriteAllText(decpath, script);
                }
                //setup_progress.Value++;
                Refresh();
            }

            Cursor.Current = Cursors.Default;

        }
    }
}
