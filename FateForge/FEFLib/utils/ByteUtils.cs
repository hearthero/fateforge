﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.utils
{
    public class ByteUtils
    {

        public static byte[] INT2LE(int data)
        {
            byte[] b = new byte[4];
            b[0] = (byte)data;
            b[1] = (byte)(((uint)data >> 8) & 0xFF);
            b[2] = (byte)(((uint)data >> 16) & 0xFF);
            b[3] = (byte)(((uint)data >> 24) & 0xFF);
            return b;
        }


        public static short toShort(byte[] encodedValues, int index)
        {
            return (short)(((encodedValues[index + 1] & 0xff) << 8) + (encodedValues[index] & 0xff));
        }

        public static int toBigEndianShort(byte[] encodedValues, int index)
        {
            return (((encodedValues[index] << 8) & 0xFF00) | (encodedValues[index + 1] & 0xFF));
        }

        public static int toInt(byte[] encodedValue, int index)
        {
            int value = (encodedValue[index + 3] << (8 * 3));
            value |= (encodedValue[index + 2] & 0xFF) << (8 * 2);
            value |= (encodedValue[index + 1] & 0xFF) << (8);
            value |= (encodedValue[index] & 0xFF);
            return value;
        }

        public static int toInt(List<byte> bytes, int start)
        {
            byte[] byteArray = new byte[4];
            for (int x = 0; x < 4; x++)
                byteArray[x] = bytes[start + x];
            return toInt(byteArray, 0);
        }

        public static byte[] toByteArray(short value)
        {
            byte[] ret = new byte[2];
            ret[0] = (byte)(value & 0xff);
            ret[1] = (byte)((value >> 8) & 0xff);
            return ret;
        }

        public static List<byte> toByteList(byte[] arr)
        {
            List<byte> bytes = new List<byte>();
            foreach (byte b in arr)
                bytes.Add(b);
            return bytes;
        }

        public static byte[] toIntByteArray(int value)
        {
            byte[] encodedValue = new byte[4];
            encodedValue[3] = (byte)(value >> (8 * 3));
            encodedValue[2] = (byte)(value >> (8 * 2));
            encodedValue[1] = (byte)(value >> 8);
            encodedValue[0] = (byte)value;
            return encodedValue;
        }

        public static List<byte> toByteList(int i) {
            byte[] bytes = INT2LE(i);
            List<byte> output = new List<byte>();
            foreach (byte b in bytes)
            output.Add(b);
            return output;
        }

        public static string getString(byte[] source, int index) {
            int end = index;
            while (source[end] != 0) {
                end++;
            }
            return Encoding.GetEncoding(932).GetString(source.Skip(index).Take(end-index).ToArray());
        }

        /**
         * Reads a pointer from the given index in the source array and then parses a
         * String at the address indicated by the pointer's value + 0x20.
         *
         * @param source The source array containing the pointer and the expected String.
         * @param index An index in source where a pointer to a String occurs.
         * @return The string that the Pointer at index points to.
         * @throws UnsupportedEncodingException An exception will occur if shift-jis encoding is not supported.
         */
        public static string getStringFromPointer(byte[] source, int index) {
        return getString(source, ByteUtils.toInt(source, index) + 0x20);
        }

        /**
         * Converts an array of bytes to an immutable list of bytes.
         *
         * @param arr The array of bytes to be converted.
         * @return An immutable list of bytes containing every byte from the given array.
         */
        public static ReadOnlyCollection<byte> byteArrayToImmutableList(byte[] arr)
        {
            List<byte> bytes = new List<byte>();
            foreach (byte b in arr)
                bytes.Add(b);
            return bytes.AsReadOnly();
        }

    }
}
