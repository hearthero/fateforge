﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FEFLib.utils;

namespace FEFLib.gamedata.dispos
{
    class FatesDispos
    {
        private List<DisposFaction> factions = new List<DisposFaction>();

        public FatesDispos(string file)
        {
            try
            {
                byte[] raw = File.ReadAllBytes(file);
                parseFactions(raw);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void parseFactions(byte[] raw)
        {
            int count = 0;
            while (ByteUtils.toInt(raw, 0x20 + count * 0xC) != 0)
            {
                factions.Add(new DisposFaction(raw, 0x20 + count * 0xC));
                count++;
            }
        }

        /**
         * Serializes the current list of factions to a binary format compatible
         * with 3DS Fire Emblem games.
         *
         * @return The serialized Dispo file.
         */
        public byte[] serialize()
        {
            Dictionary<string, int> labelMap = new Dictionary<string, int>();
            Dictionary<string, int> factionMap = new Dictionary<string, int>();
            List<byte> labelBytes = new List<byte>();
            List<int> pointerOne = new List<int>();

            // Map labels using the correct order.
            int factionStart = 0x20 + (factions.Count + 1) * 0xC;
            for (int x = 0; x < factions.Count; x++)
            {
                DisposFaction f = factions[x];
                labelMap[f.getName()] = labelBytes.Count;
                addLabel(labelBytes, f.getName());
                factionMap[f.getName()] = factionStart;
                factionStart += f.getSpawns().Count * 0x8C;
                pointerOne.Add(x * 0xC);
                pointerOne.Add(x * 0xC + 4);
            }
            foreach (DisposFaction f in factions)
            {
                int ptrStart = factionMap[f.getName()] - 0x20;
                for (int i = 0; i < f.getSpawns().Count; i++)
                {
                    int blockStart = ptrStart + i * 0x8C;
                    DisposBlock b = f.getSpawns()[i];
                    if (!labelMap.ContainsKey(b.getPid()))
                    {
                        labelMap[b.getPid()] = labelBytes.Count;
                        addLabel(labelBytes, b.getPid());
                    }
                    pointerOne.Add(blockStart);
                    for (int x = 0; x < 5; x++)
                    {
                        if (!b.getItems()[x].Equals(""))
                        {
                            if (!labelMap.ContainsKey(b.getItems()[x]))
                            {
                                labelMap[b.getItems()[x]] = labelBytes.Count;
                                addLabel(labelBytes, b.getItems()[x]);
                            }
                            pointerOne.Add(blockStart + 20 + x * 8);
                        }
                    }
                    for (int x = 0; x < 5; x++)
                    {
                        if (!b.getSkills()[x].Equals(""))
                        {
                            if (!labelMap.ContainsKey(b.getSkills()[x]))
                            {
                                labelMap[b.getSkills()[x]] = labelBytes.Count;
                                addLabel(labelBytes, b.getSkills()[x]);
                            }
                            pointerOne.Add(blockStart + 60 + x * 4);
                        }
                    }
                    if (!b.getAc().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAc()))
                        {
                            labelMap[b.getAc()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAc());
                        }
                        pointerOne.Add(blockStart + 84);
                    }
                    if (!b.getAiPositionOne().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAiPositionOne()))
                        {
                            labelMap[b.getAiPositionOne()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAiPositionOne());
                        }
                        pointerOne.Add(blockStart + 88);
                    }
                    if (!b.getMi().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getMi()))
                        {
                            labelMap[b.getMi()] = labelBytes.Count;
                            addLabel(labelBytes, b.getMi());
                        }
                        pointerOne.Add(blockStart + 92);
                    }
                    if (!b.getAiPositionTwo().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAiPositionTwo()))
                        {
                            labelMap[b.getAiPositionTwo()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAiPositionTwo());
                        }
                        pointerOne.Add(blockStart + 96);
                    }
                    if (!b.getAt().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAt()))
                        {
                            labelMap[b.getAt()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAt());
                        }
                        pointerOne.Add(blockStart + 100);
                    }
                    if (!b.getAiPositionThree().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAiPositionThree()))
                        {
                            labelMap[b.getAiPositionThree()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAiPositionThree());
                        }
                        pointerOne.Add(blockStart + 104);
                    }
                    if (!b.getMv().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getMv()))
                        {
                            labelMap[b.getMv()] = labelBytes.Count;
                            addLabel(labelBytes, b.getMv());
                        }
                        pointerOne.Add(blockStart + 108);
                    }
                    if (!b.getAiPositionFour().Equals(""))
                    {
                        if (!labelMap.ContainsKey(b.getAiPositionFour()))
                        {
                            labelMap[b.getAiPositionFour()] = labelBytes.Count;
                            addLabel(labelBytes, b.getAiPositionFour());
                        }
                        pointerOne.Add(blockStart + 112);
                    }
                }
            }

            // Begin serializing.
            int labelStart = factionStart + pointerOne.Count * 4;
            byte[] raw = new byte[labelStart + labelBytes.Count];

            Array.Copy(ByteUtils.toIntByteArray(raw.Length), 0, raw, 0, 4);
            Array.Copy(ByteUtils.toIntByteArray(factionStart - 0x20), 0, raw, 4, 4);
            Array.Copy(ByteUtils.toIntByteArray(pointerOne.Count), 0, raw, 8, 4);
            for (int x = 0; x < labelBytes.Count; x++)
            {
                raw[labelStart + x] = labelBytes[x];
            }
            for (int x = 0; x < factions.Count; x++)
            {
                DisposFaction f = factions[x];
                Array.Copy(ByteUtils.toIntByteArray(labelMap[f.getName()] + labelStart - 0x20), 0,
                        raw, 0x20 + x * 0xC, 4);
                Array.Copy(ByteUtils.toIntByteArray(factionMap[f.getName()] - 0x20), 0,
                        raw, 0x24 + x * 0xC, 4);
                Array.Copy(ByteUtils.toIntByteArray(f.getSpawns().Count), 0, raw, 0x28 + x * 0xC, 4);
                for (int y = 0; y < f.getSpawns().Count; y++)
                {
                    DisposBlock b = f.getSpawns()[y];
                    int start = factionMap[f.getName()] + y * 0x8C;
                    Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getPid()] + labelStart - 0x20), 0,
                            raw, start, 4);
                    Array.Copy(b.getUnknownOne(), 0, raw, start + 4, 4);
                    raw[start + 8] = b.getTeam();
                    raw[start + 9] = b.getLevel();
                    Array.Copy(b.getSecondCoord(), 0, raw, start + 10, 2);
                    Array.Copy(b.getFirstCoord(), 0, raw, start + 12, 2);
                    Array.Copy(b.getUnknownTwo(), 0, raw, start + 14, 2);
                    Array.Copy(b.getSpawnBitflags(), 0, raw, start + 16, 4);
                    for (int z = 0; z < 5; z++)
                    {
                        if (!b.getItems()[z].Equals(""))
                        {
                            Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getItems()[z]] + labelStart - 0x20), 0,
                                    raw, start + 20 + z * 8, 4);
                            Array.Copy(b.getItemBitflags()[z], 0, raw, start + 24 + z * 8, 4);
                        }
                    }
                    for (int z = 0; z < 5; z++)
                    {
                        if (!b.getSkills()[z].Equals(""))
                        {
                            Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getSkills()[z]] + labelStart - 0x20), 0,
                                    raw, start + 60 + z * 4, 4);
                        }
                    }
                    Array.Copy(b.getSkillFlag(), 0, raw, start + 80, 4);
                    if (!b.getAc().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAc()] + labelStart - 0x20), 0,
                                raw, start + 84, 4);
                    }
                    if (!b.getAiPositionOne().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAiPositionOne()] + labelStart - 0x20), 0,
                                raw, start + 88, 4);
                    }
                    if (!b.getMi().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getMi()] + labelStart - 0x20), 0,
                                raw, start + 92, 4);
                    }
                    if (!b.getAiPositionTwo().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAiPositionTwo()] + labelStart - 0x20), 0,
                                raw, start + 96, 4);
                    }
                    if (!b.getAt().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAt()] + labelStart - 0x20), 0,
                                raw, start + 100, 4);
                    }
                    if (!b.getAiPositionThree().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAiPositionThree()] + labelStart - 0x20), 0,
                                raw, start + 104, 4);
                    }
                    if (!b.getMv().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getMv()] + labelStart - 0x20), 0,
                                raw, start + 108, 4);
                    }
                    if (!b.getAiPositionFour().Equals(""))
                    {
                        Array.Copy(ByteUtils.toIntByteArray(labelMap[b.getAiPositionFour()] + labelStart - 0x20), 0,
                                raw, start + 112, 4);
                    }
                    Array.Copy(b.getUnknownThree(), 0, raw, start + 116, 0x18);
                }
            }
            List<int> sorted = sortPointers(raw, pointerOne, labelStart);
            for (int x = 0; x < sorted.Count; x++)
            {
                Array.Copy(ByteUtils.toIntByteArray(sorted[x]), 0, raw, factionStart + x * 4, 4);
            }
            return raw;
        }

        /**
         * Converts a string to an array of bytes and adds it to the label region
         * of the current file.
         *
         * @param bytes The bytes that make up the label region of the file.
         * @param label The string to be added to the label region.
         */
        private void addLabel(List<byte> bytes, string label)
        {
            try
            {
                foreach (byte b in Encoding.GetEncoding(932).GetBytes(label))
                    bytes.Add(b);
                bytes.Add((byte)0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        /**
         * Sorts a list of pointers to recreate the ordering used in vanilla
         * 3DS Fire Emblem dispo files.
         *
         * @param raw The raw bytes of the current file.
         * @param pointers A list containing every address to be included in pointer region one.
         * @param labelStart The address where labels begin in the current file.
         * @return A list of pointers sorted to match a typical 3DS Fire Emblem file.
         */
        private List<int> sortPointers(byte[] raw, List<int> pointers, int labelStart)
        {
            List<KeyValuePair<int, int>> labelPtrs = new List<KeyValuePair<int, int>>();
            List<int> sorted = new List<int>();
            for (int x = 0; x < factions.Count; x++)
                sorted.Add(pointers[(x * 2) + 1]);
            for (int x = 0; x < factions.Count; x++)
                sorted.Add(pointers[x * 2]);
            for (int x = factions.Count * 2; x < pointers.Count; x++)
            {
                int pointer = pointers[x];
                KeyValuePair<int, int> KeyValuePair = new KeyValuePair<int, int>(pointer, ByteUtils.toInt(raw, pointer + 0x20));
                if (KeyValuePair.Value >= labelStart - 0x20)
                {
                    labelPtrs.Add(KeyValuePair);
                }
            }


            // Sort labels.
            // Label pointers are sorted by key. Groups are sorted by value.
            int min;
            for (int i = 0; i < labelPtrs.Count; i++)
            {
                min = i;
                for (int j = i + 1; j < labelPtrs.Count; j++)
                {
                    if (labelPtrs[j].Key < labelPtrs[min].Key)
                    {
                        min = j;
                    }
                }
                KeyValuePair<int, int> temp = labelPtrs[i];
                labelPtrs[i] = labelPtrs[min];
                labelPtrs[min] = temp;
            }
            List<KeyValuePair<int, int>> sortedLabels = new List<KeyValuePair<int, int>>();
            for (int x = 0; x < labelPtrs.Count; x++)
            {
                if (sortedLabels.Contains(labelPtrs[x]))
                    continue;
                List<KeyValuePair<int, int>> grouped = new List<KeyValuePair<int, int>>();
                grouped.Add(labelPtrs[x]);
                for (int y = x + 1; y < labelPtrs.Count; y++)
                {
                    if (Object.Equals(labelPtrs[x].Value, labelPtrs[y].Value))
                        grouped.Add(labelPtrs[y]);
                }
                for (int i = 0; i < grouped.Count; i++)
                {
                    min = i;
                    for (int j = i + 1; j < grouped.Count; j++)
                    {
                        if (grouped[j].Value < grouped[min].Value)
                        {
                            min = j;
                        }
                    }
                    KeyValuePair<int, int> temp = grouped[i];
                    grouped[i] = grouped[min];
                    grouped[min] = temp;
                }
                sortedLabels.AddRange(grouped);
            }

            foreach (KeyValuePair<int, int> p in sortedLabels)
            {
                sorted.Add(p.Key);
            }
            return sorted;
        }

        public DisposFaction getByName(string input)
        {
            foreach (DisposFaction f in factions)
            {
                if (f.getName().Equals(input))
                    return f;
            }
            return null;
        }

        public List<DisposFaction> getFactions()
        {
            return factions;
        }
    }
}
