﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FEFLib.utils;
using FEFLib.gamedata.dispos;

namespace FEFLib.gamedata.dispos
{
    class DisposFaction
    {
        private string name;
        private List<DisposBlock> spawns = new List<DisposBlock>();

        public DisposFaction(byte[] raw, int start)
        {
            try
            {
                name = ByteUtils.getStringFromPointer(raw, start);
                int blockStart = ByteUtils.toInt(raw, start + 4) + 0x20;
                int count = ByteUtils.toInt(raw, start + 8);
                for (int x = 0; x < count; x++)
                {
                    spawns.Add(new DisposBlock(raw, blockStart + x * 0x8C));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public DisposFaction(string name)
        {
            this.name = name;
        }

        public bool containsByPid(string input)
        {
            foreach (DisposBlock d in spawns)
            {
                if (d.getPid().Equals(input))
                    return true;
            }
            return false;
        }

        public DisposBlock getByPid(string input)
        {
            foreach (DisposBlock d in spawns)
            {
                if (d.getPid().Equals(input))
                    return d;
            }
            return null;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public List<DisposBlock> getSpawns()
        {
            return spawns;
        }

        public void addSpawn(DisposBlock block)
        {
            spawns.Add(block);
        }
    }
}
