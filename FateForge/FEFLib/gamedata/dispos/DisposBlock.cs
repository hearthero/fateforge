﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FEFLib.utils;

namespace FEFLib.gamedata.dispos
{
    public class DisposBlock
    {
        private string pid = "Placeholder";
        private string ac = "";
        private string aiPositionOne = "";
        private string mi = "";
        private string aiPositionTwo = "";
        private string at = "";
        private string aiPositionThree = "";
        private string mv = "";
        private string aiPositionFour = "";

        private byte team = 0;
        private byte level = 0;

        private byte[] skillFlag = new byte[4];
        private byte[] unknownOne = new byte[4];
        private byte[] secondCoord = new byte[2];
        private byte[] firstCoord = new byte[2];
        private byte[] unknownTwo = new byte[2];
        private byte[] spawnBitflags = new byte[4];
        private byte[][] itemBitflags = new byte[5][];
        private byte[] unknownThree = new byte[0x18];

        private string[] items = new string[5];
        private string[] skills = new string[5];

        public DisposBlock(byte[] raw, int start)
        {
            for (int x = 0; x < 5; x++)
            {
                items[x] = "";
                skills[x] = "";
            }
            try
            {
                read(raw, start);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public DisposBlock(string pid)
        {
            for (int x = 0; x < 5; x++)
            {
                items[x] = "";
                skills[x] = "";
            }
            this.pid = pid;
        }

        public DisposBlock(DisposBlock src)
        {
            pid = src.getPid();
            ac = src.getAc();
            aiPositionOne = src.getAiPositionOne();
            mi = src.getMi();
            aiPositionTwo = src.getAiPositionTwo();
            at = src.getAt();
            aiPositionThree = src.getAiPositionThree();
            mv = src.getMv();
            aiPositionFour = src.getAiPositionFour();
            team = src.getTeam();
            level = src.getLevel();
            skillFlag = new byte[4];
            Array.Copy(src.getSkillFlag(), 0, skillFlag, 0, 4);
            unknownOne = src.getUnknownOne();
            secondCoord = src.getSecondCoord();
            firstCoord = src.getFirstCoord();
            unknownTwo = src.getUnknownTwo();
            spawnBitflags = src.getSpawnBitflags();
            itemBitflags = src.getItemBitflags();
            unknownThree = src.getUnknownThree();
            items = new string[src.getItems().Length];
            Array.Copy(src.getItems(), 0, items, 0, items.Length);
            skills = new string[src.getSkills().Length];
            Array.Copy(src.getSkills(), 0, skills, 0, skills.Length);
        }

        byte[] CopyOfRange(byte[] src, int start, int end)
        {
            int len = end - start;
            byte[] dest = new byte[len];
            for (int i = 0; i < len; i++)
            {
                dest[i] = src[start + i];
            }
            return dest;
        }

        private void read(byte[] raw, int start)
        {
            pid = ByteUtils.getStringFromPointer(raw, start);
            unknownOne = CopyOfRange(raw, start + 4, start + 8);
            team = raw[start + 8];
            level = raw[start + 9];
            secondCoord = CopyOfRange(raw, start + 10, start + 12);
            firstCoord = CopyOfRange(raw, start + 12, start + 14);
            unknownTwo = CopyOfRange(raw, start + 14, start + 16);
            spawnBitflags = CopyOfRange(raw, start + 16, start + 20);
            for (int x = 0; x < 5; x++)
            {
                if (ByteUtils.toInt(raw, start + 20 + x * 8) != 0)
                {
                    items[x] = ByteUtils.getStringFromPointer(raw, start + 20 + x * 8);
                    itemBitflags[x] = CopyOfRange(raw, start + 24 + x * 8, start + 28 + x * 8);
                }
            }
            for (int x = 0; x < 5; x++)
            {
                if (ByteUtils.toInt(raw, start + 60 + x * 4) != 0)
                {
                    skills[x] = ByteUtils.getStringFromPointer(raw, start + 60 + x * 4);
                }
            }
            skillFlag = CopyOfRange(raw, start + 80, start + 84);
            if (ByteUtils.toInt(raw, start + 84) != 0)
                ac = ByteUtils.getStringFromPointer(raw, start + 84);
            if (ByteUtils.toInt(raw, start + 88) != 0)
                aiPositionOne = ByteUtils.getStringFromPointer(raw, start + 88);
            if (ByteUtils.toInt(raw, start + 92) != 0)
                mi = ByteUtils.getStringFromPointer(raw, start + 92);
            if (ByteUtils.toInt(raw, start + 96) != 0)
                aiPositionTwo = ByteUtils.getStringFromPointer(raw, start + 96);
            if (ByteUtils.toInt(raw, start + 100) != 0)
                at = ByteUtils.getStringFromPointer(raw, start + 100);
            if (ByteUtils.toInt(raw, start + 104) != 0)
                aiPositionThree = ByteUtils.getStringFromPointer(raw, start + 104);
            if (ByteUtils.toInt(raw, start + 108) != 0)
                mv = ByteUtils.getStringFromPointer(raw, start + 108);
            if (ByteUtils.toInt(raw, start + 112) != 0)
                aiPositionFour = ByteUtils.getStringFromPointer(raw, start + 112);
            unknownThree = CopyOfRange(raw, start + 116, start + 116 + 0x18);
        }

        public string getPid()
        {
            return pid;
        }

        public void setPid(string pid)
        {
            this.pid = pid;
        }

        public string getAc()
        {
            return ac;
        }

        public void setAc(string ac)
        {
            this.ac = ac;
        }

        public string getMi()
        {
            return mi;
        }

        public void setMi(string mi)
        {
            this.mi = mi;
        }

        public string getAt()
        {
            return at;
        }

        public void setAt(string at)
        {
            this.at = at;
        }

        public string getMv()
        {
            return mv;
        }

        public void setMv(string mv)
        {
            this.mv = mv;
        }

        public byte getTeam()
        {
            return team;
        }

        public void setTeam(byte team)
        {
            this.team = team;
        }

        public byte getLevel()
        {
            return level;
        }

        public void setLevel(byte level)
        {
            this.level = level;
        }

        public byte[] getUnknownOne()
        {
            return unknownOne;
        }

        public void setUnknownOne(byte[] unknownOne)
        {
            this.unknownOne = unknownOne;
        }

        public byte[] getSecondCoord()
        {
            return secondCoord;
        }

        public void setSecondCoord(byte[] secondCoord)
        {
            this.secondCoord = secondCoord;
        }

        public byte[] getFirstCoord()
        {
            return firstCoord;
        }

        public void setFirstCoord(byte[] firstCoord)
        {
            this.firstCoord = firstCoord;
        }

        public byte[] getUnknownTwo()
        {
            return unknownTwo;
        }

        public void setUnknownTwo(byte[] unknownTwo)
        {
            this.unknownTwo = unknownTwo;
        }

        public byte[] getSpawnBitflags()
        {
            return spawnBitflags;
        }

        public void setSpawnBitflags(byte[] spawnBitflags)
        {
            this.spawnBitflags = spawnBitflags;
        }

        public byte[][] getItemBitflags()
        {
            return itemBitflags;
        }

        public void setItemBitflags(byte[][] itemBitflags)
        {
            this.itemBitflags = itemBitflags;
        }

        public string[] getItems()
        {
            return items;
        }

        public void setItem(string item, int index)
        {
            this.items[index] = item;
        }

        public string[] getSkills()
        {
            return skills;
        }

        public void setSkill(string skill, int index)
        {
            this.skills[index] = skill;
        }

        public string getAiPositionOne()
        {
            return aiPositionOne;
        }

        public void setAiPositionOne(string aiPositionOne)
        {
            this.aiPositionOne = aiPositionOne;
        }

        public string getAiPositionTwo()
        {
            return aiPositionTwo;
        }

        public void setAiPositionTwo(string aiPositionTwo)
        {
            this.aiPositionTwo = aiPositionTwo;
        }

        public string getAiPositionThree()
        {
            return aiPositionThree;
        }

        public void setAiPositionThree(string aiPositionThree)
        {
            this.aiPositionThree = aiPositionThree;
        }

        public string getAiPositionFour()
        {
            return aiPositionFour;
        }

        public void setAiPositionFour(string aiPositionFour)
        {
            this.aiPositionFour = aiPositionFour;
        }

        public byte[] getUnknownThree()
        {
            return unknownThree;
        }

        public byte[] getSkillFlag()
        {
            return skillFlag;
        }

        public void setSkillFlag(byte[] skillFlag)
        {
            this.skillFlag = skillFlag;
        }

    }
}
