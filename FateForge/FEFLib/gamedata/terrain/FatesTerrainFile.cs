﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.gamedata.terrain
{
    public class FatesTerrainFile : BinaryReader
    {
        private List<TileBlock> tiles = new List<TileBlock>();
        private string mapModel;

        private int mapSizeX;
        private int mapSizeY;
        private int borderSizeX;
        private int borderSizeY;
        private byte[,] map;

        public FatesTerrainFile(Stream file, Encoding mode) : base(file,mode)
        {
            BaseStream.Seek(0x20, SeekOrigin.Begin);
            int tableOffset = ReadInt32() + 0x20;
            
            int tableCount = ReadInt32();
            mapModel = readStringFromPointer();
            int gridOffset = ReadInt32() + 0x20;

            processFileTable(tableOffset, tableCount);
            processMap(gridOffset);
        }

        private string ReadNullTerminatedString(BinaryReader input) 
        {
            string str = "";
            char ch;
            while ((int)(ch = input.ReadChar()) != 0)
                str = str + ch;
            return str;
        }

        public string readStringFromPointer()
        {

            int offset = ReadInt32();
            if (offset <= 0)
                return "";
            int original = (int)BaseStream.Position;
            BaseStream.Seek(offset + 0x20, SeekOrigin.Begin);
            string parsedString = ReadNullTerminatedString(this);
            BaseStream.Seek(original, SeekOrigin.Begin);
            return parsedString;
        }

        private void processFileTable(int offset, int tableCount)
        {
            try
            {
                BaseStream.Seek(offset, SeekOrigin.Begin);
                for (int x = 0; x < tableCount; x++)
                {
                    TileBlock block = new TileBlock(this);
                    tiles.Add(block);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void processMap(int offset)
        {
            map = new byte[32,32];

            try
            {
                BaseStream.Seek(offset, SeekOrigin.Begin);
                mapSizeX = ReadInt32();
                mapSizeY = ReadInt32();
                borderSizeX = ReadInt32();
                borderSizeY = ReadInt32();
                BaseStream.Seek(BaseStream.Position + 8, SeekOrigin.Begin);
                for (int x = 0; x < 32; x++)
                {
                    for (int y = 0; y < 32; y++)
                        map[x,y] = ReadByte();
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void setTile(int x, int y, byte value)
        {
            map[x,y] = value;
        }

        public List<TileBlock> getTiles()
        {
            return tiles;
        }

        public string getMapModel()
        {
            return mapModel;
        }

        public void setMapModel(string mapModel)
        {
            this.mapModel = mapModel;
        }

        public int getMapSizeX()
        {
            return mapSizeX;
        }

        public void setMapSizeX(int mapSizeX)
        {
            this.mapSizeX = mapSizeX;
        }

        public int getMapSizeY()
        {
            return mapSizeY;
        }

        public void setMapSizeY(int mapSizeY)
        {
            this.mapSizeY = mapSizeY;
        }

        public int getBorderSizeX()
        {
            return borderSizeX;
        }

        public void setBorderSizeX(int borderSizeX)
        {
            this.borderSizeX = borderSizeX;
        }

        public int getBorderSizeY()
        {
            return borderSizeY;
        }

        public void setBorderSizeY(int borderSizeY)
        {
            this.borderSizeY = borderSizeY;
        }

        public byte[,] getMap()
        {
            return map;
        }

    }
}
