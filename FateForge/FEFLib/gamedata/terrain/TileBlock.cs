﻿using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.gamedata.terrain
{
    public class TileBlock
    {
        private string tid;
        private string mTid;
        private byte[] unknown;
        private byte placementNumber;
        private byte[] changeIds;
        private byte unknownByte;
        private byte defenseBonus;
        private byte avoidBonus;
        private byte healingBonus;
        private byte[] unknownTwo;
        private string effect;

        public TileBlock()
        {

        }

        public TileBlock(TileBlock t)
        {
            tid = t.getTid();
            mTid = t.getmTid();
            unknown = t.getUnknown();
            placementNumber = t.getPlacementNumber();
            changeIds = t.getChangeIds();
            unknownByte = t.getUnknownByte();
            defenseBonus = t.getDefenseBonus();
            avoidBonus = t.getAvoidBonus();
            healingBonus = t.getHealingBonus();
            unknownTwo = t.getUnknownTwo();
            effect = t.getEffect();
        }

        public TileBlock(BinaryReader file)
        {
            try
            {
                tid = readStringFromPointer(file);
                mTid = readStringFromPointer(file);
                unknown = file.ReadBytes(4);
                placementNumber = file.ReadByte();
                changeIds = file.ReadBytes(3);
                unknownByte = file.ReadByte();
                defenseBonus = file.ReadByte();
                avoidBonus = file.ReadByte();
                healingBonus = file.ReadByte();
                unknownTwo = file.ReadBytes(0x10);
                effect = readStringFromPointer(file);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private string ReadNullTerminatedString(BinaryReader input)
        {
            string str = "";
            char ch;
            while ((int)(ch = input.ReadChar()) != 0)
                str = str + ch;
            return str;
        }

        public string readStringFromPointer(BinaryReader input)
        {

            int offset = input.ReadInt32();
            if (offset <= 0)
                return "";
            int original = (int)input.BaseStream.Position;
            input.BaseStream.Seek(offset + 0x20, SeekOrigin.Begin);
            string parsedString = ReadNullTerminatedString(input);
            input.BaseStream.Seek(original, SeekOrigin.Begin);
            return parsedString;
        }

        public byte[] getBytes(int[] pointers)
        {
            if (pointers.Length < 3)
                return null;
            byte[] bytes = new byte[0x28];
            Array.Copy(ByteUtils.toIntByteArray(pointers[0]), 0, bytes, 0, 4);
            Array.Copy(ByteUtils.toIntByteArray(pointers[1]), 0, bytes, 0x4, 4);
            Array.Copy(unknown, 0, bytes, 0x8, 4);
            bytes[0xC] = placementNumber;
            Array.Copy(changeIds, 0, bytes, 0xD, 3);
            bytes[0x10] = unknownByte;
            bytes[0x11] = defenseBonus;
            bytes[0x12] = avoidBonus;
            bytes[0x13] = healingBonus;
            Array.Copy(unknownTwo, 0, bytes, 0x14, 0x10);
            Array.Copy(ByteUtils.toIntByteArray(pointers[2]), 0, bytes, 0x24, 4);
            return bytes;
        }

        public string getTid()
        {
            return tid;
        }

        public void setTid(string tid)
        {
            this.tid = tid;
        }

        public string getmTid()
        {
            return mTid;
        }

        public void setmTid(string mTid)
        {
            this.mTid = mTid;
        }

        public byte[] getUnknown()
        {
            return unknown;
        }

        public void setUnknown(byte[] unknown)
        {
            this.unknown = unknown;
        }

        public byte getPlacementNumber()
        {
            return placementNumber;
        }

        public void setPlacementNumber(byte placementNumber)
        {
            this.placementNumber = placementNumber;
        }

        public byte[] getChangeIds()
        {
            return changeIds;
        }

        public void setChangeIds(byte[] changeIds)
        {
            this.changeIds = changeIds;
        }

        public byte getUnknownByte()
        {
            return unknownByte;
        }

        public void setUnknownByte(byte unknownByte)
        {
            this.unknownByte = unknownByte;
        }

        public byte getDefenseBonus()
        {
            return defenseBonus;
        }

        public void setDefenseBonus(byte defenseBonus)
        {
            this.defenseBonus = defenseBonus;
        }

        public byte getAvoidBonus()
        {
            return avoidBonus;
        }

        public void setAvoidBonus(byte avoidBonus)
        {
            this.avoidBonus = avoidBonus;
        }

        public byte getHealingBonus()
        {
            return healingBonus;
        }

        public void setHealingBonus(byte healingBonus)
        {
            this.healingBonus = healingBonus;
        }

        public byte[] getUnknownTwo()
        {
            return unknownTwo;
        }

        public void setUnknownTwo(byte[] unknownTwo)
        {
            this.unknownTwo = unknownTwo;
        }

        public string getEffect()
        {
            return effect;
        }

        public void setEffect(string effect)
        {
            this.effect = effect;
        }

    }
}
