﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.script
{
        public class CompiledEvent
        {
        private HeaderInformation header;
        private string[] eventText;
        private int offset;
        private int subheaderOffset;
        private int eventOffset;
        private int eventStart;
        private List<byte> subheaderBytes = new List<byte>();
        private List<byte> eventBytes = new List<byte>();

        public CompiledEvent()
        {
        }

        public void compile(ScriptCompiler compiler, string[] text)
        {
            //Eliminate whitespace first.
            eventText = text;
            parseHeaderInformation();
            if (header == null)
            {
                eventBytes = new List<byte>();
                return;
            }
            for (int y = 0; y < eventText.Length; y++)
            {
                eventText[y] = eventText[y].Trim();
                if (eventText[y].StartsWith("routine"))
                    header.setRoutine();
            }

            if (header.hasSubheader())
                subheaderBytes = SubheaderParser.getSubheaderFromText(compiler, eventText);
            string[] onlyEvent = new string[eventText.Length - getEventLine()];
            Array.Copy(eventText, getEventLine(), onlyEvent, 0, eventText.Length - getEventLine());
            EventCompiler eventCompiler = new EventCompiler();
            eventBytes = eventCompiler.createEvent(compiler, onlyEvent, getEventLine() + eventStart);
        }

        private int getEventLine()
        {
            if (!header.hasSubheader())
                return 1;
            else {
                int x = 1;
                while (!eventText[x].Equals("end"))
                {
                    x++;
                }
                return x + 1;
            }
        }

        private void parseHeaderInformation()
        {
            if (eventText[0].StartsWith("Event"))
            {
                header = new HeaderInformation();
                string[] parameters = ScriptUtils.parseEmbeddedParameters(eventText[0]);
                header.setEventType(Convert.ToInt32(parameters[0],16));
                header.setHasSubheader(eventText[1].Contains("Subheader"));
                header.setUnknownCheck(Convert.ToBoolean(parameters[1]));
            }
        }

        public int getOffset()
        {
            return offset;
        }

        public void setOffset(int offset_)
        {
            offset = offset_;
        }

        public List<byte> getSubheaderBytes()
        {
            return subheaderBytes;
        }

        public List<byte> getEventBytes()
        {
            return eventBytes;
        }

        public int getSubheaderOffset()
        {
            return subheaderOffset;
        }

        public void setSubheaderOffset(int subheaderOffset_)
        {
            subheaderOffset = subheaderOffset_;
        }

        public int getEventOffset()
        {
            return eventOffset;
        }

        public void setEventOffset(int eventOffset_)
        {
            eventOffset = eventOffset_;
        }

        public void setEventStart(int eventStart_)
        {
            eventStart = eventStart_;
        }

        public HeaderInformation getHeader()
        {
            return header;
        }
    }
}
