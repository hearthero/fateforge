﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FEFLib.utils;
using System.Text.RegularExpressions;

namespace FEFLib.script
{
    public static class ScriptUtils
    {

        public static string[] parseEventParameters(string input)
        {
            if (input.Contains('('))
                input = input.Substring(input.IndexOf('(') + 1, input.Length - 1 - (input.IndexOf('(') + 1));

            List<string> preliminaryResults = new List<string>();

            while (input.Contains(','))
            {
                string nextParam;
                if (input.StartsWith("string"))
                    nextParam = input.Substring(0, input.IndexOf("\")") + 1);
                else
                    nextParam = input.Substring(0, input.IndexOf(')') + 1);
                input = input.Substring(input.IndexOf(',') + 1);
                preliminaryResults.Add(nextParam);
            }

            preliminaryResults.Add(input);
            return preliminaryResults.ToArray();
        }

        public static string[] parseEmbeddedParameters(string input)
        {
            if (input.Contains('('))
                input = input.Substring(input.IndexOf('(') + 1, input.Length - 1 - (input.IndexOf('(')+1));
            string[] split = input.Split(',');
            for (int x = 0; x < split.Length; x++)
                split[x] = split[x].Trim();
            return split;
        }

        public static string parseSingleParameter(string input)
        {
            if (input.StartsWith("string") || input.StartsWith("routine"))
                return ScriptUtils.parseStringParameter(input);
            if (input.Contains('('))
                input = input.Substring(input.IndexOf('(') + 1, input.Length - 1 - (input.IndexOf('(') + 1));
            return input;
        }

        private static string parseStringParameter(string input)
        {
            if (!(input.StartsWith("string") || input.StartsWith("routine")))
                return "";
            else {
                input = input.Replace("string", "");
                input = input.Replace("routine", "");
                input = input.Substring(1, input.Length - 2);
                if (input.StartsWith("\""))
                    input = input.Replace("\"", "");
                return input;
            }
        }

        public static short shortFromByteArray(byte[] input, int index)
        {
            byte[] bytes = new byte[2];
            int x = 0;
            while (x < 2)
            {
                bytes[x] = input[index + x];
                x++;
            }

            Array.Reverse(bytes);

            return (short)BitConverter.ToInt16(bytes,0);
            //return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getShort();
        }

        public static string readCoordBytes(byte[] input, int index)
        {
            StringBuilder result = new StringBuilder();
            for (int x = 0; x < 4; x++)
            {
                UInt32 coord = Convert.ToUInt32(input[index + x] & 0xFF);
                result.Append("0x").Append(coord.ToString("X").ToUpper());
                if (x < 3)
                    result.Append(",");
            }
            return result.ToString();
        }

        public static byte[] getSearchBytes(String input)
        {
            List<byte> bytes = new List<byte>();
            bytes.Add(0);
            foreach (byte b in Encoding.GetEncoding(932).GetBytes(input))
                bytes.Add(b);
            bytes.Add(0);

            byte[] temp = new byte[bytes.Count];
            for (int x = 0; x < bytes.Count; x++)
                temp[x] = bytes[x];
            return temp;
        }

        public static byte[] byteArrayFromList(List<byte> input)
        {
            byte[] output = new byte[input.Count];
            for (int x = 0; x < input.Count; x++)
                output[x] = input[x];
            return output;
        }

        public static int indexOf(byte[] outerArray, byte[] smallerArray)
        {
            for (int i = 0; i < outerArray.Length - smallerArray.Length + 1; ++i)
            {
                bool found = true;
                for (int j = 0; j < smallerArray.Length; ++j)
                {
                    if (outerArray[i + j] != smallerArray[j])
                    {
                        found = false;
                        break;
                    }
                }
                if (found) return i;
            }
            return -1;
        }

        public static void writeShortToList(List<byte> input, short value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            foreach (byte b in bytes)
                input.Add(b);
        }

        public static void writeShortToList(List<byte> input, short value, int index)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            for (int x = 0; x < 2; x++)
                input[index + x] = bytes[x];
        }

    }
}
