﻿using FEFLib.script;
using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class EventCompiler
    {
        private Dictionary<int, int> savedLines = new Dictionary<int, int>();
        private Stack<KeyValuePair<int, int>> conditionals = new Stack<KeyValuePair<int, int>>();
        private int lengthReduction = -1;

        public EventCompiler()
        {

        }

        public static long LDecode(string s)
        {
            return Convert.ToInt64(s, 16);
        }

        public List<byte> createEvent(ScriptCompiler compiler, string[] input, int lineNumber)
        {
            List<byte> result = new List<byte>();
            int x = 0;
            while (!input[x].StartsWith("end"))
            {
                compileLine(result, input, x, compiler, lineNumber);
                x++;
            }
            result.Add((byte)0x54);
            result.Add((byte)0);
            savedLines.Clear();
            return result;
        }

        private void compileLine(List<byte> events, string[] input, int start, ScriptCompiler compiler, int lineNumber)
        {
            savedLines[start] = events.Count;
            if (input[start].StartsWith("pass") || input[start].StartsWith("fail") || input[start].StartsWith("specialCheck")
                    || input[start].StartsWith("unknownCheck"))
            {
                updateConditionals(1);
                compileConditional(events, input, start);
            }
            else if (input[start].StartsWith("raw"))
            {
                string[] parameters = ScriptUtils.parseEmbeddedParameters(input[start]);
                foreach (string s in parameters)
                    events.Add((byte)LDecode(s));
                updateConditionals(parameters.Length);
            }
            else if (input[start].StartsWith("byte"))
            {
                byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(input[start]));
                events.Add((byte)0x19);
                events.Add(value);
                updateConditionals(2);
            }
            else if (input[start].StartsWith("short"))
            {
                short value = Convert.ToInt16(LDecode(ScriptUtils.parseSingleParameter(input[start])));
                events.Add((byte)0x1A);
                byte[] bytes = BitConverter.GetBytes(value);
                Array.Reverse(bytes);
                foreach (byte b in bytes)
                    events.Add(b);
                updateConditionals(3);
            }
            else if (input[start].StartsWith("int"))
            {
                int value = Convert.ToInt32(LDecode(ScriptUtils.parseSingleParameter(input[start])));
                events.Add((byte)0x1B);
                byte[] bytes = ByteUtils.toIntByteArray(value);
                foreach (byte b in bytes)
                    events.Add(b);
                updateConditionals(5);
            }
            else if (input[start].StartsWith("string"))
            {
                events.AddRange(compileString(input, start, compiler));
            }
            else if (input[start].StartsWith("storein"))
            {
                byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(input[start]));
                events.Add((byte)0x7);
                events.Add(value);
                updateConditionals(2);
            }
            else if (input[start].StartsWith("call"))
            {
                byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(input[start]));
                events.Add((byte)0x1);
                events.Add(value);
                updateConditionals(2);
            }
            else if (input[start].StartsWith("goto"))
            {
                compileGoTo(events, input, start, lineNumber);
                updateConditionals(3);
            }
            else if (input[start].StartsWith("followFailure"))
            { // Special case for A028 Enthusiasm events.

            }
            else if (input[start].StartsWith("omit"))
            {

            }
            else if (input[start].StartsWith("reduce"))
            {
                lengthReduction = Convert.ToInt32(ScriptUtils.parseSingleParameter(input[start]), 16);
            }
            else if (input[start].StartsWith("}"))
            {
                compileConditionalEnd(events, input, start);
            }
            else {
                parseEventLine(events, input, start, compiler);
            }
        }

        private List<byte> compileParameters(string[] parameters, ScriptCompiler compiler)
        {
            List<byte> output = new List<byte>();
            for (int y = 0; y < parameters.Length; y++)
            {
                if (parameters[y].StartsWith("string"))
                {
                    output.AddRange(compileString(parameters, y, compiler));
                }
                else if (parameters[y].StartsWith("byte"))
                {
                    byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(parameters[y]));
                    output.Add((byte)0x19);
                    output.Add(value);
                    updateConditionals(2);
                }
                else if (parameters[y].StartsWith("storein"))
                {
                    byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(parameters[y]));
                    output.Add((byte)0x7);
                    output.Add(value);
                    updateConditionals(2);
                }
                else if (parameters[y].StartsWith("call"))
                {
                    byte value = (byte)LDecode(ScriptUtils.parseSingleParameter(parameters[y]));
                    output.Add((byte)0x1);
                    output.Add(value);
                    updateConditionals(2);
                }
                else if (parameters[y].StartsWith("raw"))
                {
                    string[] byteStrings = ScriptUtils.parseEmbeddedParameters(parameters[y]);
                    foreach (string s in byteStrings)
                    {
                        output.Add((byte)LDecode(s));
                    }
                    updateConditionals(byteStrings.Length);
                }
                else if (parameters[y].StartsWith("short"))
                {
                    short value = Convert.ToInt16(LDecode(ScriptUtils.parseSingleParameter(parameters[y])));
                    byte[] bytes = BitConverter.GetBytes(value);
                    Array.Reverse(bytes);
                    output.Add((byte)0x1A);
                    foreach (byte b in bytes)
                    {
                        output.Add(b);
                    }
                    updateConditionals(3);
                }
                else if (parameters[y].StartsWith("int"))
                {
                    int value = Convert.ToInt32(LDecode(ScriptUtils.parseSingleParameter(parameters[y])));
                    output.Add((byte)0x1B);
                    byte[] bytes = ByteUtils.toIntByteArray(value);
                    foreach (byte b in bytes) {
                        output.Add(b);
                    }
                    updateConditionals(5);
                }
                else if (parameters[y].StartsWith("coord"))
                {
                    string[] byteStrings = ScriptUtils.parseEmbeddedParameters(parameters[y]);
                    output.Add((byte)0x1F);
                    foreach (string s in byteStrings)
                    {
                        output.Add((byte)LDecode(s));
                    }
                    updateConditionals(5);
                }
            }
            return output;
        }

        private List<byte> compileString(string[] input, int start, ScriptCompiler compiler)
        {
            string label = ScriptUtils.parseSingleParameter(input[start]);
            List<byte> output = new List<byte>();
            short offset;
            offset = (short)compiler.getLabelOffset(label);
            if (offset <= 0x7F)
            {
                output.Add((byte)0x1C);
                output.Add((byte)offset);
                updateConditionals(2);
            }
            else {
                output.Add((byte)0x1D);
                byte[] bytes = BitConverter.GetBytes(offset);
                Array.Reverse(bytes);
                foreach (byte b in bytes)
                    output.Add(b);
                updateConditionals(3);
            }
            return output;
        }

        private void parseEventLine(List<byte> events, string[] input, int start, ScriptCompiler compiler)
        {
            string[] parameters = ScriptUtils.parseEventParameters(input[start]);
            events.AddRange(compileParameters(parameters, compiler));
            string label = ScriptUtils.parseEmbeddedParameters(input[start].Substring(0, input[start].IndexOf('(')))[0];
            short offset = (short)compiler.getLabelOffset(label);
            byte[] bytes = BitConverter.GetBytes(offset);
            Array.Reverse(bytes);
            savedLines[start] = events.Count;
            events.Add((byte)0x47);
            foreach (byte b in bytes)
                events.Add(b);
            if (ScriptSingleton.getInstance().getTags().ContainsKey(label))
            {
                events.AddRange(ScriptSingleton.getInstance().getTags()[label]);
                updateConditionals(ScriptSingleton.getInstance().getTags()[label].Length);
            }
            updateConditionals(3);
        }

        private void compileConditional(List<byte> events, string[] input, int start)
        {
            conditionals.Push(new KeyValuePair<int, int>(events.Count, 2)); // Minimum condition length is 2, so initialize with that value.
            if (input[start].StartsWith("pass"))
                events.Add((byte)0x4C);
            else if (input[start].StartsWith("fail"))
                events.Add((byte)0x49);
            else if (input[start].StartsWith("specialCheck"))
                events.Add((byte)0x4D);
            else if (input[start].StartsWith("unknownCheck"))
                events.Add((byte)0x4B);
            ScriptUtils.writeShortToList(events, (short)0);
        }

        private void compileConditionalEnd(List<byte> events, string[] input, int start)
        {
            KeyValuePair<int, int> conditional = conditionals.Pop();
            bool hasEndConditional = false;
            if (input[start + 1].StartsWith("fail") || input[start - 1].StartsWith("followFailure"))
            {
                conditional = new KeyValuePair<int, int>(conditional.Key, conditional.Value + 3);
                hasEndConditional = true;
            }
            if (lengthReduction != -1)
                conditional = new KeyValuePair<int, int>(conditional.Key, conditional.Value - lengthReduction);
            ScriptUtils.writeShortToList(events, Convert.ToInt16(conditional.Value), conditional.Key + 1);
            if (lengthReduction != -1)
            {
                conditional = new KeyValuePair<int, int>(conditional.Key, conditional.Value + lengthReduction);
                lengthReduction = -1;
            }

            List<KeyValuePair<int, int>> temp = new List<KeyValuePair<int, int>>();
            for (int x = 0; x < conditionals.Count; x++)
            {
                KeyValuePair<int, int> previous = conditionals.Pop();
                if (x == 0) // Only the next conditional needs to know the length being added to it. The rest will be updated later.
                {
                    if (hasEndConditional)
                        previous = new KeyValuePair<int, int>(previous.Key, previous.Value + conditional.Value - 3);
                    else
                        previous = new KeyValuePair<int, int>(previous.Key, previous.Value + conditional.Value);
                }
                temp.Add(previous);
            }
            for (int x = temp.Count - 1; x >= 0; x--)
                conditionals.Push(temp[x]);
        }

        private void compileGoTo(List<byte> events, string[] input, int start, int lineNumber)
        {
            int targetLine = Int32.Parse(ScriptUtils.parseSingleParameter(input[start])) //Get the target line relative to the array.
                    - lineNumber;
            short targetIndex = (short)(((events.Count + 2) - Convert.ToInt16(savedLines[targetLine]) + 1) * -1);
            events.Add((byte)0x49);
            ScriptUtils.writeShortToList(events, targetIndex);
        }

        private void updateConditionals(int length)
        {
            if (conditionals.Count > 0)
            {
                KeyValuePair<int, int> conditional = conditionals.Pop();
                conditional = new KeyValuePair<int, int>(conditional.Key, conditional.Value + length);
                conditionals.Push(conditional);
            }
        }
    }
}
