﻿using FEFLib.script;
using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class EventDecompiler
    {
        private readonly byte[] INTEGER_VALS = { 0x01, 0x07, 0x19, 0x1A, 0x1B, 0x1F };
        private readonly byte[] CONDITIONAL_VALS = { 0x49, 0x4C, 0x4D, 0x4B };
        private readonly byte[] STRING_VALS = { 0x1C, 0x1D };
        private readonly byte[] RAW_VALS = { 0x50, 0x46, 0x03 };
        private readonly byte EVENT_BYTE = 0x47;

        private List<string> decompiled = new List<string>();
        private List<KeyValuePair<string, int>> parameters = new List<KeyValuePair<string,int>>();
        private Dictionary<int, int> savedPoints = new Dictionary<int, int>();
        private Stack<int> conditionalIndexes = new Stack<int>();
        private Stack<int> conditionalLengths = new Stack<int>();
        private Stack<bool> specialCaseStack = new Stack<bool>();
        private int nests = 0;
        private int lineNumber;
        private bool omitFlag = false;
        private int reductionNumber = -1; // Special case for B027.

        public EventDecompiler()
        {
        }

        public List<string> parseEventString(byte[] input, ScriptDecompiler decompiler, int line)
        {
            lineNumber = line;
            for (int x = 0; x < input.Length; x++)
            {
                KeyValuePair<string, int> command = new KeyValuePair<string, int>();
                processConditionals(input, x);
                if (omitFlag)
                {
                    decompiled.Add(getWhiteSpace() + "omit");
                    omitFlag = false;
                }
                if (input[x] == EVENT_BYTE)
                {
                    if (parameters.Count > 0 && !decompiler.getFileName().StartsWith("A002_OP1"))
                    {
                        if (parameters[0].Key.StartsWith("raw"))
                        {
                            int index = parameters[0].Value;
                            savedPoints[index] = decompiled.Count;
                            decompiled.Add(getWhiteSpace() + parameters[0].Key);
                            parameters.RemoveAt(0);
                        }
                    }
                    command = parseEventCommand(input, x, decompiler);
                    parameters.Clear();
                    savedPoints.Add(x, decompiled.Count);
                    decompiled.Add(getWhiteSpace() + command.Key);
                }
                foreach (byte b in RAW_VALS)
                {
                    if (command.Value != 0)
                        break;
                    if (input[x] == b)
                    {
                        command = parseKnownRaw(input, x, decompiler);
                        parameters.Add(new KeyValuePair<string,int>(command.Key, x));
                        savedPoints.Add(x, decompiled.Count);
                        break;
                    }
                }
                foreach (byte b in INTEGER_VALS)
                {
                    if (command.Value != 0)
                        break;
                    if (input[x] == b)
                    {
                        command = parseIntegerValue(input, x);
                        parameters.Add(new KeyValuePair<string,int>(command.Key, x));
                        savedPoints.Add(x, decompiled.Count);
                        break;
                    }
                }
                foreach (byte b in STRING_VALS)
                {
                    if (command.Value != 0)
                        break;
                    if (input[x] == b)
                    {
                        command = parseString(input, x, decompiler);
                        parameters.Add(new KeyValuePair<string,int>(command.Key, x));
                        savedPoints.Add(x, decompiled.Count);
                        break;
                    }
                }
                foreach (byte b in CONDITIONAL_VALS)
                {
                    if (command.Value != 0)
                        break;
                    if (input[x] == b)
                    {
                        if (x + 2 < input.Length)
                        {
                            short val = ScriptUtils.shortFromByteArray(input, x + 1);
                            if (val < 0)
                            {
                                dumpParameters();
                                savedPoints.Add(x, decompiled.Count);
                                command = parseReturn(input, x);
                                decompiled.Add(getWhiteSpace() + command.Key);
                                break;
                            }
                        }
                        dumpParameters();
                        savedPoints.Add(x, decompiled.Count);
                        command = parseCondition(input, x);
                        decompiled.Add(getWhiteSpace() + command.Key);
                        nests++;
                        break;
                    }
                }
                if (command.Equals(default(KeyValuePair<string, int>))) // If no known byte sequence is found, pool unknown bytes into their own parameter.
                {
                    command = parseRaw(input, x);
                    parameters.Add(new KeyValuePair<string,int>(command.Key, x));
                    savedPoints.Add(x, decompiled.Count);
                }
                x += command.Value;
            }

            processConditionals(input, input.Length);
            nests = 0;
            conditionalLengths.Clear();
            conditionalIndexes.Clear();
            savedPoints.Clear();
            dumpParameters();
            lineNumber = 0;
            return decompiled;
        }

        private KeyValuePair<string, int> parseCondition(byte[] input, int index)
        {
            String result = "";
            int advance = 2;
            int length = (int)ScriptUtils.shortFromByteArray(input, index + 1);
            if (input[index] == 0x49)
                result += "fail {";
            else if (input[index] == 0x4C)
                result += "pass {";
            else if (input[index] == 0x4D)
                result += "specialCheck {";
            else if (input[index] == 0x4B)
                result += "unknownCheck {";
            KeyValuePair<bool, int> endData = hasEndConditional(input);
            conditionalLengths.Push(length);
            conditionalIndexes.Push(index + 1);
            KeyValuePair<bool, int> newEndData = hasEndConditional(input);
            if (endData.Key && newEndData.Key) // Special case scenario for A028 and B028.
            {
                if (endData.Value == newEndData.Value && length == 5)
                {
                    conditionalLengths.Pop();
                    conditionalIndexes.Pop();
                    conditionalLengths.Push(length - 3);
                    conditionalIndexes.Push(index + 1);
                    specialCaseStack.Push(true);
                    return new KeyValuePair<string,int>(result, advance);
                }
            }
            specialCaseStack.Push(false);
            return new KeyValuePair<string,int>(result, advance);
        }

        private KeyValuePair<string, int> parseEventCommand(byte[] input, int index, ScriptDecompiler decompiler)
        {
            StringBuilder result = new StringBuilder(decompiler.parseString(ByteUtils.toBigEndianShort(input, index + 1)));
            int advance = 2;
            try
            {
                if (ScriptSingleton.getInstance().getTags().ContainsKey(result.ToString()))
                {
                    advance += ScriptSingleton.getInstance().getTags()[result.ToString()].Length;
                    byte[] tag = ScriptSingleton.getInstance().getTags()[result.ToString()];
                    for (int x = 0; x < tag.Length; x++)
                    {
                        if (input[index + 3 + x] != tag[x])
                            Console.WriteLine("Mismatched tag on " + result.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            // Build event line from parsed information.
            result.Append("(");
            for (int x = 0; x < parameters.Count; x++)
            {
                result.Append(parameters[x].Key);
                if (x < parameters.Count - 1)
                    result.Append(",");
            }
            result.Append(")");

            return new KeyValuePair<string,int>(result.ToString(), advance);
        }

        private static KeyValuePair<string, int> parseIntegerValue(byte[] input, int index)
        {
            string result = "";
            int advance = 0;
            byte command = input[index];
            long longhex = 0;
            //ulong ulonghex;

            if (command == 0x01 || command == 0x07 || command == 0x19)
            {
                advance = 1;
            }
            else if (command == 0x1A)
            {
                advance = 2;
            }
            else if (command == 0x1B)
            {
                advance = 4;
            }
            else if (command == 0x1F)
            {
                advance = 4;
            }
            if (command == 0x01)
            {
                longhex = input[index + 1] & 0xFF;
                result += "call(0x" + longhex.ToString("X").ToUpper() + ")";
            }
            else if (command == 0x07)
            {
                longhex = input[index + 1] & 0xFF;
                result += "storein(0x" + longhex.ToString("X").ToUpper() + ")";
            }
            else if (command == 0x19)
            {
                longhex = input[index + 1] & 0xFF;
                result += "byte(0x" + longhex.ToString("X").ToUpper() + ")";
            }
            else if (command == 0x1A)
            {
                longhex = ScriptUtils.shortFromByteArray(input, index + 1);
                result += "short(0x" + longhex.ToString("X").ToUpper() + ")";
            }
            else if (command == 0x1B)
            {
                longhex = Convert.ToInt64(ByteUtils.toInt(input, index + 1));
                result += "int(0x" + longhex.ToString("X").ToUpper() + ")";
            }
            else if (command == 0x1F)
            {
                result += "coord(" + ScriptUtils.readCoordBytes(input, index + 1) + ")";
            }
            return new KeyValuePair<string,int>(result, advance);
        }

        private static KeyValuePair<string, int> parseString(byte[] input, int index, ScriptDecompiler decompiler)
        {
            String result = "string(\"";
            int advance = 0;
            byte command = input[index];
            if (command == 0x1C)
            {
                result += decompiler.parseString(input[index + 1]);
                advance = 1;
            }
            else if (command == 0x1D)
            {
                result += decompiler.parseString(ByteUtils.toBigEndianShort(input, index + 1));
                advance += 2;
            }
            result += "\")";
            return new KeyValuePair<string,int>(result, advance);
        }

        private KeyValuePair<string, int> parseKnownRaw(byte[] input, int index, ScriptDecompiler decompiler)
        {
            String result = "";
            int advance = 0;
            byte command = input[index];
            if (command == 0x46)
            {
                if (decompiler.getFileName().Equals("Command.cmb"))
                {
                    result += parseRaw(input, index, 3).Key;
                    advance += 2;
                }
                else {
                    result += parseRaw(input, index, 2).Key;
                    advance = 1;
                }
            }
            else if (command == 0x03)
            {
                if (input[index + 1] != 0x47 && !(decompiler.getFileName().Equals("007.cmb")
                        || decompiler.getFileName().Equals("012.cmb")))
                {
                    result += parseRaw(input, index, 2).Key;
                    advance = 1;
                }
                else {
                    result += parseRaw(input, index, 1).Key;
                    advance = 0;
                }
            }
            else if (command == 0x50)
            {
                result += parseRaw(input, index, 2).Key;
                advance = 1;
            }
            return new KeyValuePair<string, int>(result, advance);
        }

        private KeyValuePair<string, int> parseRaw(byte[] input, int index)
        {
            StringBuilder result = new StringBuilder();
            int advance = 0;
            List<byte> raw = new List<byte>();
            while (!isKnownByte(input[index + advance]))
            {
                raw.Add(input[index + advance]);
                advance++;
                if (index + advance >= input.Length)
                    break;
                if (conditionalLengths.Count > 0 && conditionalIndexes.Count > 0)
                {
                    if (index + advance >= conditionalLengths.Peek() + conditionalIndexes.Peek())
                        break;
                }
            }
            advance--;

            result.Append("raw(");
            for (int x = 0; x < raw.Count; x++)
            {
                int hexvalue = raw[x] & 0xFF;
                result.Append("0x").Append(hexvalue.ToString("X").ToUpper());
                if (x < raw.Count - 1)
                    result.Append(",");
                else
                    result.Append(")");
            }
            
            return new KeyValuePair<string,int>(result.ToString(), advance);
        }

        private KeyValuePair<string, int> parseRaw(byte[] input, int index, int length)
        {
            StringBuilder result = new StringBuilder();
            int advance = length - 1;
            List<byte> raw = new List<byte>();
            for (int x = 0; x < length; x++)
                raw.Add(input[index + x]);
            result.Append("raw(");
            for (int x = 0; x < raw.Count; x++)
            {
                int hexvalue = raw[x] & 0xFF;
                result.Append("0x").Append(hexvalue.ToString("X").ToUpper());
                if (x < raw.Count - 1)
                    result.Append(",");
                else
                    result.Append(")");
            }
            return new KeyValuePair<string,int>(result.ToString(), advance);
        }

        private bool isKnownByte(byte input)
        {
            bool isKnownByte = false;
            if (input == EVENT_BYTE)
                return true;
            foreach (byte b in INTEGER_VALS)
            {
                if (input == b)
                    isKnownByte = true;
            }
            foreach (byte b in CONDITIONAL_VALS)
            {
                if (input == b)
                    isKnownByte = true;
            }
            foreach (byte b in STRING_VALS)
            {
                if (input == b)
                    isKnownByte = true;
            }
            foreach (byte b in RAW_VALS)
            {
                if (input == b)
                    isKnownByte = true;
            }
            return isKnownByte;
        }

        private void dumpParameters()
        {
            String result;
            foreach (KeyValuePair<string, int> s in parameters)
            {
                int index = s.Value;
                savedPoints[index] = decompiled.Count;
                result = getWhiteSpace() + s.Key;
                decompiled.Add(result);
            }
            parameters.Clear();
        }

        private KeyValuePair<string, int> parseReturn(byte[] input, int index)
        {
            String result;
            int advance = 2;
            int returnPoint = (ScriptUtils.shortFromByteArray(input, index + 1) + 3) * -1; // Return markers will always use a negative value.
            if (savedPoints.ContainsKey(index - returnPoint))
            {
                int point = savedPoints[index - returnPoint];
                int line = point + lineNumber;

                //Special case fix for A002_OP1 bev file.
                if (input[index - returnPoint] != 0x47 && (decompiled[point].StartsWith("ev::") || decompiled[point].StartsWith("bev::")))
                {
                    return parseRaw(input, index, 3);
                }

                result = "goto(" + line + ")";
                return new KeyValuePair<string,int>(result, advance);
            }
            else {
                return parseRaw(input, index, 3);
            }
        }

        private void processConditionals(byte[] input, int currentIndex)
        {
            if (conditionalLengths.Count > 0 && conditionalIndexes.Count > 0)
            {
                int currentConditionalLength = conditionalLengths.Peek();
                int currentConditionalIndex = conditionalIndexes.Peek();
                KeyValuePair<bool, int> endData = hasEndConditional(input);
                if (endData.Key && endData.Value == currentIndex)
                {
                    breakNest();
                }
                else if ((currentConditionalIndex + currentConditionalLength) <= currentIndex)
                {
                    if (currentIndex + 3 < input.Length) // Fix for P006.
                    {
                        if (input[currentIndex] == 0x49 && input[currentIndex + 0x3] == 0x49)
                        {
                            short value = ScriptUtils.shortFromByteArray(input, currentIndex + 1);
                            if (value > 0)
                                omitFlag = true;
                        }
                    }
                    breakNest();
                    if (conditionalLengths.Count > 0) // Fix for B027 and B028.
                    {
                        int nextConditionalLength = conditionalLengths.Pop();
                        int nextConditionalIndex = conditionalIndexes.Pop();
                        int thirdConditionalLength;
                        int thirdConditionalIndex;
                        if (reductionNumber != -1)
                        {
                            currentConditionalLength += reductionNumber;
                            reductionNumber = -1;
                        }
                        int extraLength = (currentConditionalLength + currentConditionalIndex) - (nextConditionalIndex + nextConditionalLength);
                        if (extraLength > 0)
                        {
                            decompiled.Add(getWhiteSpace() + "reduce(" + extraLength.ToString("X") + ")");

                            if (conditionalLengths.Count > 0)
                            {
                                thirdConditionalLength = conditionalLengths.Peek();
                                thirdConditionalIndex = conditionalIndexes.Peek();
                                extraLength = (currentConditionalLength + currentConditionalIndex) - (thirdConditionalIndex + thirdConditionalLength);
                                if (extraLength > 0)
                                {
                                    reductionNumber = extraLength;
                                }
                            }
                        }
                        conditionalLengths.Push(nextConditionalLength);
                        conditionalIndexes.Push(nextConditionalIndex);
                    }
                    processConditionals(input, currentIndex);
                }
            }
        }

        private KeyValuePair<bool, int> hasEndConditional(byte[] input)
        {
            if (conditionalLengths.Count > 0 && conditionalIndexes.Count > 0)
            {
                int currentConditionalLength = conditionalLengths.Peek();
                int currentConditionalIndex = conditionalIndexes.Peek();
                int checkIndex = currentConditionalLength + currentConditionalIndex - 3;
                if (checkIndex < input.Length)
                {
                    foreach (byte b in CONDITIONAL_VALS)
                    {
                        if (ScriptUtils.shortFromByteArray(input, checkIndex + 1) < 0)
                            return new KeyValuePair<bool, int>(false, checkIndex); // The ending is a goto.
                        if (input[checkIndex] == b)
                            return new KeyValuePair<bool, int>(true, checkIndex); // Ends in an else statement.
                    }
                }
            }
            return new KeyValuePair<bool, int>(false, -1);
        }

        private String getWhiteSpace()
        {
            StringBuilder result = new StringBuilder();
            for (int x = 0; x < nests; x++)
                result.Append("    ");
            return result.ToString();
        }

        private void breakNest()
        {
            if (specialCaseStack.Peek())
            {
                decompiled.Add(getWhiteSpace() + "followFailure");
            }
            conditionalLengths.Pop();
            conditionalIndexes.Pop();
            specialCaseStack.Pop();
            dumpParameters();
            nests--;
            decompiled.Add(getWhiteSpace() + "}");
        }
    }
}
