﻿using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class SubheaderParser
    {
        public static List<byte> getSubheaderFromText(ScriptCompiler compiler, string[] input)
        {
            List<byte> result = new List<byte>();
            int x = 1;
            while (!input[x].StartsWith("end"))
            {
                if (input[x].StartsWith("int"))
                {
                    string parsed = ScriptUtils.parseSingleParameter(input[x]);
                    if (parsed.ToLower().Equals("ffffffff"))
                    {
                        for (int y = 0; y < 4; y++)
                            result.Add(0xFF);
                    }
                    else {
                        int parsedInt = Convert.ToInt32(parsed, 16);
                        byte[] bytes = ByteUtils.toIntByteArray(parsedInt);
                        foreach (byte b in bytes)
                        {
                            result.Add(b);
                            Console.WriteLine("int: "+ b);
                        }
                    }
                }
                else if (input[x].StartsWith("string"))
                {
                    string s = ScriptUtils.parseSingleParameter(input[x]);
                    int offset;
                    offset = compiler.getLabelOffset(s);
                    byte[] bytes = ByteUtils.toIntByteArray(offset);
                    foreach (byte b in bytes)
                    {
                        result.Add(b);
                        Console.WriteLine("string: "+ b);
                    }
                }
                else if (input[x].StartsWith("routine"))
                {
                    string s = ScriptUtils.parseSingleParameter(input[x]);
                    byte[] stringBytes = Encoding.GetEncoding(932).GetBytes(s);
                    foreach (byte b in stringBytes)
                    {
                        result.Add(b);
                        Console.WriteLine("routine: " + b);
                    }
                    result.Add(0);
                }
                x++;
            }
            return result;
        }
    }
}
