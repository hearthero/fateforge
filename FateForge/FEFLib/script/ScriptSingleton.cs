﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FEFLib.script
{
    public class ScriptSingleton
    {
        private static ScriptSingleton instance = new ScriptSingleton();

        private Dictionary<string, byte[]> tags = new Dictionary<string, byte[]>();
        private Dictionary<int, byte[]> subheaders = new Dictionary<int, byte[]>();

        public static ScriptSingleton getInstance()
        {
            return instance;
        }

        private ScriptSingleton()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(FateForge.Properties.Resources.Commands);
                doc.DocumentElement.Normalize();
                XmlNodeList nList = doc.GetElementsByTagName("Command");
                for (int x = 0; x < nList.Count; x++)
                {
                    XmlNode node = nList.Item(x);
                    if (!node.Attributes.GetNamedItem("name").Value.Equals(""))
                    {
                        if (node.Attributes.GetNamedItem("tag").Value.Equals(""))
                        {
                            tags[node.Attributes.GetNamedItem("name").Value] = new byte[0];
                        }
                        else {
                            string[] splitString = node.Attributes.GetNamedItem("tag").Value.Split(',');
                            byte[] bytes = new byte[splitString.Length];
                            for (int y = 0; y < bytes.Length; y++)
                            {
                                bytes[y] = Convert.ToByte(splitString[y],16);
                            }
                            tags[node.Attributes.GetNamedItem("name").Value] = bytes;
                        }
                    }
                }

                doc.LoadXml(FateForge.Properties.Resources.Subheaders);
                doc.DocumentElement.Normalize();
                nList = doc.GetElementsByTagName("Subheader");
                for (int x = 0; x < nList.Count; x++)
                {
                    XmlNode node = nList.Item(x);
                    if (!node.Attributes.GetNamedItem("id").Value.Equals(""))
                    {
                        int id = Convert.ToInt32(node.Attributes.GetNamedItem("id").Value);
                        string[] unparsedValues = node.Attributes.GetNamedItem("layout").Value.Split(',');
                        byte[] values = new byte[unparsedValues.Length];
                        for (int y = 0; y < values.Length; y++)
                            values[y] = Convert.ToByte(unparsedValues[y],16);
                        subheaders[id] = values;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public Dictionary<string, byte[]> getTags()
        {
            return tags;
        }

        public Dictionary<int, byte[]> getSubheaders()
        {
            return subheaders;
        }

    }
}
