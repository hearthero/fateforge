﻿using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class ScriptDecompiler
    {

        private int reference;
        private List<string> result;
        private byte[] bytes;
        private string fileName;

        public ScriptDecompiler()
        {
        }

        public string decompile(string path) {

            byte[] testlist = ByteUtils.toByteArray((short)100);

            foreach (byte b in testlist)
            {
                Console.WriteLine(b);
            }

            fileName = Path.GetFileName(path);
            List<int> headers = new List<int>();
            result = new List<string>();
            bytes = File.ReadAllBytes(path);
            int pointerOffset = ByteUtils.toInt(bytes, 0x1C);
            reference = ByteUtils.toInt(bytes, 0x20);

            int count = 0;
            int temp;
            while ((temp = ByteUtils.toInt(bytes, pointerOffset + count * 4)) != 0)
            {
                headers.Add(temp);
                count++;
            }

            for (int x = 0; x < headers.Count; x++)
            {
                int i = headers[x];
                int evtOffset = ByteUtils.toInt(bytes, i + 4);
                int type = ByteUtils.toInt(bytes, i + 8);
                int unknown = ByteUtils.toInt(bytes, i + 0x10);
                int subOffset = ByteUtils.toInt(bytes, i + 0x14);
                int evtEnd;
                if (x != headers.Count - 1)
                    evtEnd = headers[x + 1];
                else
                    evtEnd = reference;
                bool unknownCheck = (unknown == 0 && subOffset == 0);
                bool hasSub = (evtOffset != subOffset) && (!unknownCheck);
                byte[] evtBytes = bytes.Skip(evtOffset).Take(evtEnd - evtOffset).ToArray();

                // Cut off the end signifier.
                int endIndex = -1;
                for (int y = evtBytes.Length - 1; y > -1; y--)
                {
                    if (evtBytes[y] == 0x54)
                    {
                        endIndex = y;
                        break;
                    }
                }
                byte[] trimmedEvtBytes = new byte[endIndex];
                Array.Copy(evtBytes, 0, trimmedEvtBytes, 0, endIndex);

                result.Add("Event ev" + x + "(0x" + type.ToString("X").ToUpper() + "," + unknownCheck + ")");

                // Decompile subheader.
                if (hasSub)
                {
                    result.Add("Subheader");
                    if (unknown != 0)
                        result.Add("routine(\"" + ByteUtils.getString(bytes, i + 0x18) + "\")");
                    else
                        decSubheader(bytes.Skip(subOffset).Take(evtOffset-subOffset).ToArray(), type);
                    result.Add("end");
                }

                // Decompile event.
                EventDecompiler evtDecompiler = new EventDecompiler();
                int line = result.Count + headers.IndexOf(i) + 1;
                result.AddRange(evtDecompiler.parseEventString(trimmedEvtBytes, this, line));
                result.Add("end\n");
            }
            headers.Clear();

            StringBuilder res = new StringBuilder();
            foreach (string s in result)
                res.Append(s).Append(Environment.NewLine);

            res = res.Replace("raw(0x0)"+Environment.NewLine, "");
            res = res.Replace("False", "false");

            return res.ToString();
        }

        private void decSubheader(byte[] subBytes, int evType) {
            if (ScriptSingleton.getInstance().getSubheaders().ContainsKey(evType))
            {
                byte[] vals = ScriptSingleton.getInstance().getSubheaders()[evType];
                for (int x = 0; x < vals.Length; x++)
                {
                    int val = Convert.ToInt32(ByteUtils.toInt(subBytes, x * 4));
                    if (vals[x] == 0)
                    {
                        result.Add("int(0x" + val.ToString("X").ToUpper() + ")");
                    }
                    else {
                        string res = ByteUtils.getString(bytes, val + reference);
                        if (res.Equals(""))
                            res = "null";
                        result.Add("string(\"" + res + "\")");
                    }
                }
                return;
            }
            for (int x = 0; x < subBytes.Length / 4; x++)
            {
                int val = Convert.ToInt32(ByteUtils.toInt(subBytes, x * 4));
                if (isPointer(bytes, val))
                {
                    string res = ByteUtils.getString(bytes, val + reference);
                    if (res.Equals(""))
                        res = "null";
                    result.Add("string(\"" + res + "\")");
                }
                else {
                    result.Add("int(0x" + val.ToString("X").ToUpper() + ")");
                }
            }
        }

        private bool isPointer(byte[] bytes, int offset)
        {
            return offset >= 0 && offset + reference < bytes.Length && offset != 0 && bytes[offset + reference - 1] == 0;
        }

        public string parseString(int offset)
        {
            return ByteUtils.getString(bytes, reference + offset);
        }

        public string getFileName()
        {
            return fileName;
        }

    }
}
