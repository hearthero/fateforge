﻿using FEFLib.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class ScriptCompiler
    {

        private List<CompiledEvent> events = new List<CompiledEvent>();
        private List<byte> labels = new List<byte>();
        private List<byte> data;
        private int pointerStart;
        private int lineNumbers = 1;
        private int nullCounter = 0;
        private string fileName;

        public ScriptCompiler(string fileName)
        {
            data = new List<byte>();
            labels.Add(0x0);
            this.fileName = fileName;
        }

        public void compile(string path, string input)
        {
            string[] unsplitEvents = Regex.Split(input, "\\r?\\n\\r?\\n");
            writeHeader(unsplitEvents.Length);
            if (input.Equals(""))
            {
                for (int x = 0; x < 4; x++)
                    data.RemoveAt(data.Count - 1);
                finish(path);
                return;
            }
            foreach (string unsplitEvent in unsplitEvents)
            {
                if (unsplitEvent != "")
                {
                    CompiledEvent e = new CompiledEvent();
                    string[] temp = Regex.Split(unsplitEvent, "\\r?\\n");
                    e.setEventStart(lineNumbers);
                    events.Add(e);
                    lineNumbers += temp.Length + 1;
                    e.setOffset(data.Count);
                    e.compile(this, temp);

                    for (int x = 0; x < 0x18; x++)
                        data.Add(0);
                    e.setSubheaderOffset(data.Count);
                    data.AddRange(e.getSubheaderBytes());
                    e.setEventOffset(data.Count);
                    data.AddRange(e.getEventBytes());
                    if (events.IndexOf(e) != unsplitEvents.Length - 1)
                    {
                        while (data.Count % 4 != 0)
                            data.Add(0);
                    }

                    byte[] offsetBytes = ByteUtils.toIntByteArray(e.getOffset());
                    byte[] typeBytes = ByteUtils.toIntByteArray(e.getHeader().getEventType());
                    byte[] evtOffsetBytes = ByteUtils.toIntByteArray(e.getEventOffset());
                    byte[] indexBytes = ByteUtils.toIntByteArray(events.IndexOf(e));
                    byte[] subheaderOffsetBytes = ByteUtils.toIntByteArray(e.getSubheaderOffset());
                    for (int x = 0; x < 4; x++)
                    {
                        data[e.getOffset() + x] = offsetBytes[x];
                        data[e.getOffset() + 0x4 + x] = evtOffsetBytes[x];
                        data[e.getOffset() + 0x8 + x] = typeBytes[x];
                        data[e.getOffset() + 0xC + x] = indexBytes[x];
                        if (e.getHeader().isRoutine())
                        {
                            data[e.getOffset() + 0x10 + x] = subheaderOffsetBytes[x];
                        }
                        else if (e.getHeader().hasSubheader())
                        {
                            data[e.getOffset() + 0x10 + x] = 0;
                            data[e.getOffset() + 0x14 + x] = subheaderOffsetBytes[x];
                        }
                        else if (!e.getHeader().unknownCheck())
                        {
                            data[e.getOffset() + 0x14 + x] = evtOffsetBytes[x];
                        }
                    }
                }
            }
            finish(path);
        }

        private void finish(string path)
        {
            int reference = data.Count;
            labels.RemoveAt(0);
            data.AddRange(labels);
            for (int x = 0; x < nullCounter; x++)
                data.Add(0);
            byte[] ptrStartBytes = ByteUtils.toIntByteArray(pointerStart);
            byte[] refBytes = ByteUtils.toIntByteArray(reference);
            for (int x = 0; x < 4; x++)
            {
                data[0x1C + x] = ptrStartBytes[x];
                data[0x20 + x] = refBytes[x];
            }
            for (int x = 0; x < events.Count; x++)
            {
                byte[] temp = ByteUtils.toIntByteArray(events[x].getOffset());
                for (int y = 0; y < 4; y++)
                {
                    data[pointerStart + x * 4 + y] = temp[y];
                }
            }
            File.WriteAllBytes(path + Path.DirectorySeparatorChar + "A007.cmb", ScriptUtils.byteArrayFromList(data));
        }

        private void writeHeader(int totalEvents)
        {

            byte[] header = {0x63, 0x6D, 0x62, 0x00, 0x19, 0x08, 0x11, 0x20, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            foreach (byte b in header)
                data.Add(b);
            for (int x = 0; x < 0xC; x++)
                data.Add(0);
            data.AddRange(ByteUtils.toByteList(Encoding.GetEncoding(932).GetBytes(fileName)));
            data.Add(0);
            while (data.Count % 4 != 0)
                data.Add(0);
            pointerStart = data.Count;
            Console.WriteLine(pointerStart);
            for (int x = 0; x < totalEvents + 1; x++)
            {
                for (int y = 0; y < 4; y++)
                    data.Add(0);
            }
        }

        public int getLabelOffset(string s)
        {
            int insertOffset = labels.Count - 1; // Cut off the leading 0.
            List<byte> searchBytes = new List<byte>();
            if (!s.Equals("null"))
            {
                byte[] labelBytes = Encoding.GetEncoding(932).GetBytes(s);
                foreach (byte b in labelBytes)
                    searchBytes.Add(b);
                searchBytes.Add(0x0);

                int labelOffset = ScriptUtils.indexOf(ScriptUtils.byteArrayFromList(labels), ScriptUtils.getSearchBytes(s));
                if (labelOffset != -1)
                    return labelOffset;
                else {
                    labels.AddRange(searchBytes);
                    return insertOffset;
                }
            }
            else {
                searchBytes.Add(0x0);
                searchBytes.Add(0x0);
                int labelOffset = ScriptUtils.indexOf(ScriptUtils.byteArrayFromList(labels),
                        ScriptUtils.byteArrayFromList(searchBytes));
                if (labelOffset != -1)
                    return labelOffset;
                else {
                    nullCounter++;
                    labels.Add(0x0);
                    return insertOffset;
                }
            }
        }

    }
}
