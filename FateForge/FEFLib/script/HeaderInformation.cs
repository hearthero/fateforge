﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEFLib.script
{
    public class HeaderInformation
    {
        private bool hasSubheader_;
        private bool isRoutine_ = false;
        private bool unknownCheck_ = false;
        private int eventType_;

        public HeaderInformation() { }

        public int getEventType()
        {
            return eventType_;
        }

        public void setEventType(int eventType)
        {
            this.eventType_ = eventType;
        }

        public bool hasSubheader()
        {
            return hasSubheader_;
        }

        public void setHasSubheader(bool hasSubheader)
        {
            hasSubheader_ = hasSubheader;
        }

        public bool isRoutine()
        {
            return isRoutine_;
        }

        public void setRoutine()
        {
            isRoutine_ = true;
        }

        public bool unknownCheck()
        {
            return unknownCheck_;
        }

        public void setUnknownCheck(bool unknownCheck)
        {
            unknownCheck_ = unknownCheck;
        }
    }
}
