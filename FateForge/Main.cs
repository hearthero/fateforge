﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CTR;
using System.Threading;
using System.Media;
using System.Diagnostics;
using DSDecmp.Formats.Nitro;
using System.Text.RegularExpressions;
using ScintillaNET;
using FEFLib.script;
using FEFLib.gamedata.terrain;
using FEFLib.gamedata.dispos;

using Ted.TileEditor;

namespace FateForge
{
    public partial class Main : Form
    {

        public volatile int threads = 0;
        private ScriptCompiler compiler;
        public TreeNode previousSelectedNode = null;

        public Main()
        {
            InitializeComponent();
            Text = "FateForge - " + Globals.Project.Dir;
        }

        // UI Alerts
        internal static DialogResult Alert(params string[] lines)
        {
            SystemSounds.Asterisk.Play();
            string msg = string.Join(Environment.NewLine + Environment.NewLine, lines);
            return MessageBox.Show(msg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        internal static DialogResult Prompt(MessageBoxButtons btn, params string[] lines)
        {
            SystemSounds.Question.Play();
            string msg = string.Join(Environment.NewLine + Environment.NewLine, lines);
            return MessageBox.Show(msg, "Prompt", btn, MessageBoxIcon.Asterisk);
        }

        private void Playtest()
        {
            Process.Start(Properties.Settings.Default.path_citra, Path.Combine(Globals.Project.Dir, Globals.Project.Name + ".3ds"));
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (threads > 0) { Alert("Please wait for all operations to finish first."); return; }

            Enabled = false;
            UseWaitCursor = true;
            new Thread(() =>
            {
                threads++;
                CTR_ROM.buildROM(false, "Nintendo", Path.Combine(Globals.Project.Dir, "ExtractedExeFS"), Path.Combine(Globals.Project.Dir, "ExtractedRomFS"), Path.Combine(Globals.Project.Dir, "DecryptedExHeader.bin"), "CTR-P-BFZE", Path.Combine(Globals.Project.Dir, Globals.Project.Name + ".3ds"), status_progress, status_label);
                BeginInvoke((Action)delegate ()
                {
                    Enabled = true;
                    UseWaitCursor = false;
                    DialogResult dialogResult = MessageBox.Show("Finished compiling.\nTest ROM in CITRA?", "Compile successful", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Playtest();
                    }
                });
                threads--;
            }).Start();

        }

        private void btn_citra_Click(object sender, EventArgs e)
        {
            Playtest();
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            if (Globals.Project.Init == 0)
            {
                UI_initalize init_dialog = new UI_initalize();
                Refresh();
                init_dialog.ShowDialog();
            }

            string GameData = File.ReadAllText(Path.Combine(Globals.Folder_Encrypted,"m","@U","GameData.txt"));
            Regex rgx = new Regex(@"^MCID_(.*?): (.*?)$",RegexOptions.Multiline);


            foreach (Match match in rgx.Matches(GameData))
                Globals.GameData_Chapters.Add(match.Groups[1].Value, match.Groups[2].Value);

            rgx = new Regex(@"^(MTID_.*?): (.*?)$", RegexOptions.Multiline);


            foreach (Match match in rgx.Matches(GameData))
                Globals.GameData_Terrain.Add(match.Groups[1].Value, match.Groups[2].Value);

            TreeNode root = new TreeNode(Globals.Project.Name, 0, 0);
            map_tree.Nodes.Add(root);

            string map_path = Path.Combine(Globals.Project.Dir, "ExtractedRomFS", "GameData", "Dispos");
            string[] map_folders = Directory.GetDirectories(map_path);

            foreach (string folder in map_folders)
            {
                string title = Path.GetFileName(folder);

                switch (title)
                {
                    case "A":
                        title = "A (Birthright)";
                    break;
                    case "B":
                        title = "B (Conquest)";
                    break;
                    case "C":
                        title = "C (Revelation)";
                    break;
                }

                TreeNode mapdir = new TreeNode(title, 1, 1);
                mapdir.Name = Path.GetFileName(folder);

                string[] mapfiles = Directory.GetFiles(folder);

                foreach (string file in mapfiles)
                {
                    string filename = Path.GetFileNameWithoutExtension(file);
                    string filekey = filename.Substring(0, filename.Length - 4);

                    if (Globals.GameData_Chapters.ContainsKey(filekey))
                        mapdir.Nodes.Add(file, Globals.GameData_Chapters[filekey+"_PREFIX"] + ": " + Globals.GameData_Chapters[filekey], 2, 2);
                    else
                        mapdir.Nodes.Add(file, filekey, 2, 2);
                }

                root.Nodes.Add(mapdir);

            }

            foreach (string file in Directory.GetFiles(map_path))
            {
                string filename = Path.GetFileNameWithoutExtension(file);
                string filekey = filename.Substring(0, filename.Length - 4);

                if (Globals.GameData_Chapters.ContainsKey(filekey))
                    root.Nodes.Add(file, Globals.GameData_Chapters[filekey + "_PREFIX"] + ": " + Globals.GameData_Chapters[filekey], 2, 2);
                else
                    root.Nodes.Add(file, filekey, 2, 2);
            }

            root.Expand();
        }

        private void map_tree_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            string file = Path.GetFileNameWithoutExtension(e.Node.Name);
            Globals.GameData_Chapters[file.Substring(0, file.Length - 4)] = e.Label;
            status_label.Text = "Added MCID-Entry for \"" + file.Substring(0, file.Length - 4) + "\"";
        }

        private void map_tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.ImageIndex == 2)
            {

                chapter_control.load_data(e.Node.Name);
                // ENTER

                if (previousSelectedNode != null)
                {
                    previousSelectedNode.BackColor = map_tree.BackColor;
                    previousSelectedNode.ForeColor = map_tree.ForeColor;
                }
            }

        }

        private void map_tree_Validating(object sender, CancelEventArgs e)
        {
            if (map_tree.SelectedNode != null)
            {
                map_tree.SelectedNode.BackColor = SystemColors.Highlight;
                map_tree.SelectedNode.ForeColor = Color.White;
                previousSelectedNode = map_tree.SelectedNode;
            }
        }

    }
}
