﻿using System.Collections.Generic;

namespace FateForge
{
    internal class FFP
    {
        public string Name { get; set; }

        public string Dir { get; set; }

        public int Init { get; set; }

        public List<object> Settings { get; set; }
    }

    static class Globals
    {
        public static FFP Project;

        public static string Folder_Decrypted;
        public static string Folder_Encrypted;

        public static Dictionary<string, string> GameData_Chapters = new Dictionary<string, string>();
        public static Dictionary<string, string> GameData_Terrain = new Dictionary<string, string>();

    }
}